
var CronJob = require('cron').CronJob;
var xpElasticETL = require('../../king_modules/xpElasticETL')
module.exports = function (app) {
    var ESTemplates = app.models.commondir
    ESTemplates.getEScalls(function (err, data) {
      data.children.forEach(function (template) {
        ESTemplates.getEScallsData(template.name
        , function (err, temp) {
        if(!err){
            // console.log(temp.crone)
            if(temp.hasOwnProperty('crone')){
                console.log("crone: ", temp.crone.ss , temp.crone.mm, temp.crone.hh, temp.crone.day, temp.crone.month, temp.crone.week)
                executeJob(temp)
            }
            // console.log("Error: ", temp)
        }
        })
      })

    })

var executeJob = function(job){
    var ctime = job.crone.ss+' '+ job.crone.mm+' '+job.crone.hh+' '+job.crone.day+' '+job.crone.month+' '+job.crone.week;
    console.log(ctime)
     new CronJob(ctime, function () {
         console.log("running crone job! ", ctime)
    xpElasticETL.exeIDIMStoES(job)
    }, null, true, 'America/Los_Angeles')
}

}
