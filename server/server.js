var loopback = require('loopback');
var boot = require('loopback-boot');
var lb_ds_op = require('../king_modules/lbdsoperations')
var kingcsv = require('../king_modules/kingcsv')
var kingimport = require('../king_modules/kingimport')
var idimstopms = require('../king_modules/idimstopms')
var xpElasticETL = require('../king_modules/xpElasticETL')
var jsonfile = require('jsonfile')

var fs = require('fs')
var app = module.exports = loopback();
var path = require('path');
app.use(loopback.static(path.resolve(__dirname, '../client')));

app.start = function () {
  // start the web server
  return app.listen(function () {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);

    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function (err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module) {
    //app.start();
    app.io = require('socket.io')(app.start());
    app.io.on('connection', function (socket) {
      //kingcsv.readCSVfile('./csvcontainer/csvs/1460556821016-695.csv')


      console.log('a user connected');
      //idimstopms.addDataElements(app, 75, "neap", function (data) {
      //
      //  console.log("Success!! ", data)
      //})

      //console.log(app.models['TmpAllControlroom2'].definition.rawProperties)

      socket.on('reqGetModels', function (data) {
        console.log("socket: reqGetModels")
        app.io.emit('resGetModels', Object.keys(app.models))
      });
      socket.on('reqGetModelProperties', function (model) {
        app.io.emit('resGetModelProperties', app.models[model].definition.properties)
      });
      socket.on('reqAddDataElements', function (model) {
        idimstopms.addDataElements(app, 74, "idims", function (err, resp) {
          if (!err)
            console.log("resp: ", resp)
          else
            console.log("error: ", err)
        })
      });
      socket.on('reqCSVHeaders', function (file) {
        console.log(file)
        kingcsv.readCSVHeaders(file, function (err, headers) {
          //console.log(headers)
          app.io.emit('resCSVHeaders', headers)
        })
        //app.io.emit('resGetModelProperties', app.models[model].definition.properties)
      });
      socket.on('reqInstalledDS', function (data) {
        lb_ds_op.getDSString('./server/datasources.json', function (err, obj) {
          app.io.emit('resInstalledDS', obj)
        })
      });
      socket.on('reqDSModels', function (data) {
        lb_ds_op.discModelDef(data, app, function (err, obj) {
          console.log("Error: ", err, " Obj: ", obj)
          app.io.emit('resDSModels', {err: err, data: obj})
        })
      });
      socket.on('reqMigrateToNewTable', function (newtbl) {
        //console.log(data);
        //var dSource = app.dataSource.MES;
        //var pms_import_log = dSource.models["PmsImportLog"]
        //console.log(dSource.models)
        kingimport.importIDIMSAPIData(app, newtbl.datasetid, newtbl.url, newtbl.tablename, newtbl.campaign, newtbl.calltype, function (data) {


          app.io.emit('resMigrateToNewTable', data)
          //console.log("Success!! ", data)
          kingimport.addToDataValue(newtbl.datasetid, newtbl.tablename, newtbl.campaign, data.query, data.delquery, function (err, resp) {
            if(err){
              console.log(err, resp)
            } else
            {

              //pms_import_log.create({
              //    "sourcetype": resp.calltype,
              //    "source": resp.url,
              //    "rowsdeleted": 0,
              //    "rowsinserted": 0,
              //    "timeToFetchData": resp.timeTakenSec,
              //    "timeToDelRows": 0,
              //    "timeToInsertRows": 0,
              //    "totalImportTime": 0,
              //    "tmpTableName": resp.tableName,
              //    "user": -1,
              //    "datasetid": 0,
              //    "periodid": 0
              //  },
              //  function (err, obj) {
              //    if (!err) {
              //      process.stdout.write("Posting " + row + " bytes\r");
              //    } else {
              //      console.log("Failed  posting ")
              //      console.log(row)
              //      console.log(err)
              //    }
              //  })
              console.log("RESP: ", resp)
            }

          })
        })

        //migrateToNewTable(app, data.url, data.model);
      });
      socket.on('reqMigrateToES', function(data){
        xpElasticETL.discoverAndMigrateToES(data)
      })
      socket.on('reqModelProperties', function (data) {
        lb_ds_op.getModelProperties(data, app, function (err, obj) {
          app.io.emit('resModelProperties', {err: err, data: obj})
        })
      });
      socket.on('reqDiscoverAndBuildModel', function (data) {
        lb_ds_op.discAndBuildModel(data.table, data.datasource, app, function (err, obj) {
          app.io.emit('resDiscoverAndBuildModel', {err: err, data: obj})
        })
      });
      socket.on('reqCreateImportTemplate', function (data) {
        kingimport.createTemplate(data, function (err, resp) {
          console.log("Error: ", err, "| Resp: ", resp)
          kingimport.readAllTemplates(function (err, resp) {
            app.io.emit('resReadAllTemplates', resp)
          })
        })
      });
      socket.on('reqReadAllTemplates', function (data) {
        kingimport.readAllTemplates(function (err, resp) {
          app.io.emit('resReadAllTemplates', resp)
        })
      });
      socket.on('reqExistingDataSources', function (data) {
        lb_ds_op.getdatasources('./server/datasources.json', function (err, ds) {
          lb_ds_op.getDSString('./server/datasources.json', function (err, dskeys) {
            //console.log(obj)
            app.io.emit("resExistingDataSources", {"keys": dskeys, "ds": ds})
          })
        })
      })
      socket.on('reqAddDataSource', function (data) {
        lb_ds_op.adddatasource('./server/datasources.json', data.name, data.ds, function (err, resp) {
          console.log("DS Added: ", data.name)
          app.io.emit("resAddDataSource", data.name)
        })
      })
      socket.on('reqUpdateAngularSDK', function () {
        var exec = require('child_process').exec;
        var cmd = 'lb-ng server/server.js client/js/lb-services.js';

        exec(cmd, function (error, stdout, stderr) {
          if (!error) {
            console.log("SDK : ", stdout)
            app.io.emit("resUpdateAngularSDK", stdout)
          }
        });
      });

      socket.on('addESCron', function(esCron){
        xpElasticETL.addESCron(esCron, function(boards){
          console.log(boards)
        })
      })

      socket.on('exeIDIMStoES', function(esCron){
        xpElasticETL.exeIDIMStoES(esCron)
      })
      socket.on('reqDoImportUsingTemplate', function (data) {
        kingimport.DoImportUsingTemplate(data.template, data.model, data.csvfile, app, function (err, responce) {

          if (err.length > 0) {
            //app.io.emit('resErrorinImportUsingTemplate', {err: err, resp: responce})
            console.error("Error in import: ", err)
          }
          //console.log("Errors: ", err)
          console.log("progress: ", responce.progress)
          //  app.io.emit('ErrorLog', log)
          //  console.log("94: " , log)
          //})

          //app.io.emit('resErrorinImportUsingTemplate', {err: err, resp: responce})
          //console.log("98: ", responce)
          //console.log("Error: on record Number: ", resp, " Error Description: ", err)
          //else {
          app.io.emit('resDoImportUsingTemplate', responce)

          //}
        })
      });
    })
  }

  //  lb_ds_op.getdatasources('./server/datasources.json', function(err, obj){
  //  console.log(obj)
  //})


});
