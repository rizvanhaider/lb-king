angular.module('kingDS').controller('importunitdataCtrl',
  function ($scope, $uibModal, Notification, csvService, CSVContainner, Program, mySocket, Programstagedataelement,Dataelement, Trackedentityattribute) {

  $scope.isNoErrorinImport = true
  $scope.ImportErrorObjects = ""

  $scope.updatemodelAtriments = function(idx, type){
    console.log(idx, type)
    $scope.modelAtriments[idx].type = type
    console.log($scope.modelAtriments)
  }

    $scope.addAttributeNElementdata = function () {

    }
    //depreciated function
  $scope.saveelebutes = function (modelAtriments) {
    modelAtriments.forEach(function (catagory) {
      console.log(catagory)
      if(catagory.type === "Attribute"){
        Trackedentityattribute.create({
          "name": catagory.csvcolumn,
          "created": new Date().toUTCString(),
          "trackedentityid": 1,
          "programid": 1
        }, function(success){
          console.info(success)
        },function(err){
          console.log("error: ", err)
        })
      } else if(catagory.type === "DataElement"){
        console.log("Adding DataElement")
        Dataelement.create({
          "name": catagory.csvcolumn,
          "aggregationtype": "TRACKER",
          "disaggregationCombinationid": 0,
          "domaintype": "TRACKER",
          "lastupdated": new Date().toUTCString(),
          "zeroissignificant": true,
          "uid": "-1",
          "optionsetid": 0,
          "legendsetid": 0,
          "created": new Date().toUTCString(),
          "userid": -1,
          "publicaccess": "false",
          "commentoptionsetid": 0
        }, function(success){
          console.info(success)
        })

      } else if(catagory.type === "Both"){
        console.log("Adding Attribute & Dataelement")
        Trackedentityattribute.create({
          "name": catagory.csvcolumn,
          "created": new Date().toUTCString(),
          "trackedentityid": 1,
          "programid": 1
        }, function(success){
          console.info(success)
        }, function(err){
          console.log("Error: ", err)
        });
        Dataelement.create({
          "name": catagory.csvcolumn,
          "aggregationtype": "TRACKER",
          "disaggregationCombinationid": 0,
          "domaintype": "TRACKER",
          "lastupdated": new Date().toUTCString(),
          "zeroissignificant": true,
          "uid": "-1",
          "optionsetid": 0,
          "legendsetid": 0,
          "created": new Date().toUTCString(),
          "userid": -1,
          "publicaccess": "false",
          "commentoptionsetid": 0
        }, function(success){
          console.info(success)
        }, function(err){
          console.log("Error: ", err)
        });
      }
    })
  }

    $scope.postTrackedEntityInstance = function (csv) {
      console.log(csv)
      CSVContainner.readCSV({name: csv}, function(resp){

        console.info("Data: ", resp.data.length)
      })
    }

  Program.find(function (data) {
    $scope.dataSets = data;
  })
  $scope.addProgram = function () {

    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'states/models/addProgram.html',
      controller: "addProgramCtrl",
      size: 'md'
    });
  }
  $scope.mappingSources = ['CSV', 'API', 'Database']

  mySocket.emit('reqGetModels', null)
  mySocket.on('resGetModels', function (models) {
    $scope.ALL_MODELS = models
    //console.log(models)
  })
  $scope.mappingTemplate = {}
  $scope.mappingTemplate.delimiter = ","
  $scope.mappingTemplate.noheader = false
  $scope.mappingTemplate.updateduplicate = false
  $scope.mappingTemplate.skipduplicate = false

  $scope.items = ['item1', 'item2', 'item3'];
  $scope.animationsEnabled = true;
  $scope.CSVdata = csvService.data

  $scope.updateduplicated = $scope.skipduplicated = false
  $scope.setprimarykey = function (update, skip, pkey) {
    $scope.mappingTemplate.updateduplicate = update
    $scope.mappingTemplate.skipduplicate = skip
    $scope.mappingTemplate.primarykey = pkey
    console.log(update, skip, pkey)
  }

  $scope.getmodelproperties = function (model) {
    mySocket.emit('reqGetModelProperties', model)

  }
  mySocket.on('resGetModelProperties', function (data) {
    console.log(data)
    $scope.modelProperties = Object.keys(data)
    $scope.mappingTemplate["target"] = Object.keys(data)
    $scope.source = [];
  })

  $scope.logdata = function (file) {
    console.log(file)
    //mySocket.emit('reqCSVHeaders', file)
    csvService.data.getcsvtojson(file)
    CSVContainner.getJSON({
      name: file
    }, function (data) {
      console.log("data from sdk: ", data)
      $scope.csvcolumnswithdata = data
      $scope.csvcolumnsonly = data.data
      $scope.modelAtriments = [];
      $scope.csvcolumnsonly.forEach(function(col){
        $scope.modelAtriments.push({"csvcolumn": col, "type": ""})
      })
      console.info($scope.mappingTemplate)
    })
  }
  //mySocket.on('resCSVHeaders', function(data){
  //  $scope.csvcolumnsonly = data
  //})

  $scope.open = function (size) {

    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'states/models/upload-csv.html',
      controller: 'csvUploadModelCtrl',
      size: size,
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });
  }

  $scope.opencsvsettings = function () {

    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'states/models/csv-settings.html',
      controller: 'csvsettingsCtrl',
      size: 'md',
      resolve: {
        mappingtemplate: function () {
          return $scope.mappingTemplate;
        }
      }
    });
  }


  $scope.sourceChanged = function (col, i) {
    $scope.source[i] = col
  }
  mySocket.emit('reqReadAllTemplates')
  mySocket.on('resReadAllTemplates', function (templates) {
    $scope.existingtemplates = templates
  })
  var createtemplate = $scope.createtemplate = function (file, model, templatename) {

    $scope.mappingTemplate["name"] = templatename
    $scope.mappingTemplate["nature"] = "csv"
    $scope.mappingTemplate["file"] = file
    $scope.mappingTemplate["model"] = model
    $scope.mappingTemplate["source"] = $scope.source
    console.log($scope.mappingTemplate)
    console.log($scope.source)
    mySocket.emit('reqCreateImportTemplate', $scope.mappingTemplate)
  }
  $scope.importwithSelected = function (template, model, csvfile) {
    if (template != undefined) {
      mySocket.emit('reqDoImportUsingTemplate', {
        template: template + ".json",
        model: model,
        csvfile: csvfile
      })
      Notification.info('Importing data from ' + csvfile);
    } else
      Notification.error("Please select template")
  }
  mySocket.on('resDoImportUsingTemplate', function (res) {
    //Notification.success('Successfully Imported! '+  res.total+ ", Rows Added: "+ res.totalsuccess);

  })

  mySocket.on('resErrorinImportUsingTemplate', function (res) {
    console.error(res.err)
    //$scope.isNoErrorinImport= false
    //$scope.ImportErrorObjects= $scope.ImportErrorObjects + res.err
    Notification.error("Error: ! " + res.err);
    //Notification.error('Error Description: '+  res.resp+ " Rows Added");
  })
  //mySocket.on('ErrorLog', function(log){
  //  $scope.importErrors = log
  //})
  $scope.importErrors = []
  mySocket.on('liveErrorsKingCSV', function (liveErrorsLingCSV) {
    $scope.importErrors.push(liveErrorsLingCSV.err)
    console.log(liveErrorsLingCSV.progress)
  })
  mySocket.on('importProgress', function (progress) {
    $scope.csvlog = progress
    $scope.impProgress = progress.percentage
    if (progress.total == progress.done)
      Notification.success('Successfully Imported! ' + progress.total + ", Rows ");
  })
  mySocket.on('invalidCSVTemplate', function () {
    Notification.error('Invalid CSV Template!');
  })
});
angular.module('kingDS').controller('addProgramCtrl', function ($uibModalInstance, $scope, Program) {
    $scope.ok = function () {
      //csvService.data.updatefile($scope.selected.item)
      Program.create({
        "name": $scope.programname,
        "description": $scope.programdesc,
        "created": new Date().toUTCString()
      }, function (success) {
        console.log(success)
      })
      $uibModalInstance.close();
    };
    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };


  }
);
