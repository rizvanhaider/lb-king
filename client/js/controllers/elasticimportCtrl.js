angular.module('kingDS')
.controller('editescalltemplateCtrl', function($scope, calltoEdit, Commondir){
  $scope.idims =  calltoEdit.callData

  $scope.savechanges = function (_call) {


    Commondir.createElasticTemplate({
      data: _call
    }, function (data) {
      console.log("Success", data)
    }, function (err) {
      console.log(err)
    })
  }

})
.controller('elasticimportCtrl', function ($scope, $log, mySocket, Notification, $uibModal, Commondir) {
  $scope.pingES = function () {
    console.log("pinging....")
    mySocket.emit('pingES')
  }
  $scope.addCall = function (_call) {
    console.log($scope.idims)

    Commondir.createElasticTemplate({
      data: _call
    }, function (data) {
      console.log("Success", data)
    }, function (err) {
      console.log(err)
    })
  }
  getESCalls()

  function getESCalls() {
    $scope.escalls = []
    Commondir.getEScalls(function (data) {
      console.log(data)
      data.data.children.forEach(function (template) {
        Commondir.getEScallsData({
          path: template.name
        }, function (temp) {
          $scope.escalls.push(temp.data)
        })
      })

    })
  }
  $scope.executeCall = function () {
    console.log($scope.idims)
    mySocket.emit('exeIDIMStoES', $scope.idims)
  }
  $scope.fetchApiData = function (obj) {
    console.log(obj)
     mySocket.emit('exeIDIMStoES', obj)
    // mySocket.emit('reqMigrateToES', obj);
    // Notification.info("Calling API")
  }
  $scope.advanceEdit = function (_call) {
    var editCallInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: './states/models/advance.edit.escalls.html',
      controller: 'editescalltemplateCtrl',
      controllerAs: '$scope',
      size: 'lg',
      resolve: {
        calltoEdit: function () {
          return {
            "callData": _call
          };
        }
      }
    });
    console.log(_call)
  }
  $scope.validateAPICall = function (newCall) {
    $scope.datatypes = []
    $log.log(newCall)

    Commondir.getIDIMSkeys({
      data: newCall.url,
      type: newCall.catagory
    }, function (data) {
      $scope.IDIMSkeys = data.keys
      newCall.apiProperties = data.keys
      data.keys.forEach(function (dtype) {
        $scope.isapivalidated = true;
        $scope.showkeyproperties = true;
        $scope.datatypes.push(dtype.datatype)
      })
    }, function (err) {
      Notification.error("Error: Inavlid URL")
    })
  }


})
