angular.module('kingDS').controller('addDatasourceCtrl', function ($scope, mySocket, Notification) {
  mySocket.emit('reqExistingDataSources')
  mySocket.on('resExistingDataSources', function (dsources) {
    $scope.existingDSKeys = dsources.keys
    $scope.existingDS = dsources.ds
  })

  $scope.addDataSource = function (name, ds) {
    if (name != null && ds != null) {
      mySocket.emit('reqAddDataSource', {"name": name, "ds": ds})
    } else
      Notification.info("Invalid data!")
  }
  mySocket.on('resAddDataSource', function (dsresp) {
    Notification.success("Data Source " + dsresp + " Added")
    $scope.NewDS = {}
  })
});
