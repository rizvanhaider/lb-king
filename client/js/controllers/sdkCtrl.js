angular.module('kingDS').controller('sdkCtrl', function ($scope, Notification, mySocket) {

    $scope.updateAngularSDK = function () {
        mySocket.emit('reqUpdateAngularSDK')
    }
    mySocket.on('resUpdateAngularSDK', function(output){
        Notification.success("AngularJS SDK updated!", output)
    })

})