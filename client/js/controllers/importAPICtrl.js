angular.module('kingDS').controller('importAPICtrl', function ($http, $scope, $uibModal, Notification, mySocket, Dataset, IdimsApiCalls, Commondir, Period) {

  $scope.showAdvanceOpt = false;

  Dataset.find(function (data) {
    $scope.dataSets = data;
  })

  $scope.posturl = function (url) {
    mySocket.emit('migrateToNewTable', url);

  }
  getIdimsApiCalls()
  function getIdimsApiCalls() {
    $scope.idimscall = []
    Commondir.IDIMScallTemplates(function (data) {
      //console.log(data.data.children)
      data.data.children.forEach(function (template) {
        Commondir.IDIMScallTemplate({path: template.name}, function (temp) {
          $scope.idimscall.push(temp.data)
          //console.log(temp.data)
        })
      })

      //IdimsApiCalls.find(function(idimscalls){
      //  $scope.idimscall = idimscalls;
      //})
    })
  }

  $scope.periods = Period.find()
  $scope.fetchApiData = function (obj) {
    mySocket.emit('reqMigrateToNewTable', obj);
    Notification.info("Calling API")
    //$scope.isAPIfetchingData= true;
  }
  mySocket.on('resMigrateToNewTable', function (resp) {
    Notification.success(resp.tableName + " Updated")
    //$scope.isAPIfetchingData= false;
  })
  $scope.editApiCall = function (obj) {
    $scope.newCall = obj
    $scope.updatedata = true
  }

  $scope.editApiCallData = function (obj) {
    IdimsApiCalls.prototype$updateAttributes(
      {id: obj.id},
      obj
    );

    //$scope.deleteCallById(obj.id)
    //$scope.addNewCall(obj)
    Notification.warning("API Call Updated!");
  }

  $scope.deleteCallById = function (indx) {
    console.info("Deleting ", indx)
    Commondir.deleteIDIMScallTemplate({filename: $scope.idimscall[indx].tablename}, function (data) {
      Notification.error("Deleted Successfully");
      console.log(data)
      getIdimsApiCalls()
    })
  }

  $scope.saveTemplate = function (newCall) {
    //newCall.addDataElement = false
    Commondir.createIDIMScallTemplate({data: newCall}, function (data) {
      console.log("Success", data)
      getIdimsApiCalls()
      Notification.success("Success! Template for IDIMS Call Saved")
    }, function (err) {
      Notification.error("Error on saving template ")
      console.log(err)
    })

  }

  //this function reads and detects the data types of all keys included in API data.
  $scope.validateAPICall = function (newCall) {
    $scope.datatypes = []
    console.log(newCall)

    Commondir.getIDIMSkeys({data: newCall.url, type : newCall.calltype}, function (data) {
      $scope.IDIMSkeys = data.keys
      newCall.apiProperties = data.keys
      data.keys.forEach(function (dtype) {
        $scope.isapivalidated = true;
        $scope.showkeyproperties = true;
        $scope.datatypes.push(dtype.datatype)
      })
    }, function (err) {
      Notification.error("Error: Inavlid URL")
    })


    //IdimsApiCalls.create(newCall, function(err, resp){
    //  Notification.success("New Call Added!");
    //  console.info("err: ", err, " resp: ", resp)
    //  getIdimsApiCalls()
    //}, function(err){
    //  Notification.error("Error! "+ err.data.error.detail);
    //  console.error("Post Error: ",  err.data.error.detail)
    //})
  }
  $scope.keys = [];
  mySocket.on("datakeys", function (data) {
    $scope.keys = data;
    console.log(data);
  })
  mySocket.on("APIError", function (data) {
    alert(data);
  })
  mySocket.on("serverStatus", function (data) {
    $scope.connectionProperties = data;
    console.log(data);
  })
  $scope.addDataset = function () {

    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'states/models/addDataset.html',
      controller: "addDatasetCtrl",
      size: 'md'
    });
  }
  $scope.deleteAPITemplate = function () {

    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'states/models/deleteDialog.html',
      controller: "deleteAPITemplateCtrl",
      size: 'md'
    });
  }

  $scope.addPeriod = function () {

    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'states/models/addPeriod.html',
      controller: "addPeriodCtrl",
      size: 'md'
    });
  }


});
angular.module('kingDS').controller('addDatasetCtrl', function addDatasetCtrl($uibModalInstance, $scope, Dataset) {
    $scope.ok = function () {
      //csvService.data.updatefile($scope.selected.item)
      Dataset.create({
        "name": $scope.dataset,
        "periodtypeid": 0,
        "description": $scope.datasetdesc,
        "created": new Date().toUTCString()
      }, function (success) {
        console.log(success)
      })
      $uibModalInstance.close();
    };
    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };


  }
);
angular.module('kingDS').controller('addPeriodCtrl', function addDatasetCtrl($uibModalInstance, Notification, $scope, Period, Periodtype) {
  $scope.periodTypes = Periodtype.find();
    $scope.ok = function () {
      //csvService.data.updatefile($scope.selected.item)
      Period.create(
        {"periodtypeid": $scope.typeid,
          "startdate": $scope.startDate,
          "enddate": $scope.endDate,
          "createdBy": -1,
          "code": $scope.code,
          "year": $scope.year,
          "month": $scope.month,
          "createdDate": new Date().toUTCString()})
        , function (success) {
        Notification.success("New Period " + $scope.code+ " Added")
        console.log(success)
      }
      $uibModalInstance.close();
    };
    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };


  }
);
angular.module('kingDS').controller('deleteAPITemplateCtrl', function addDatasetCtrl($uibModalInstance, Notification, $scope, Commondir) {
  $scope.periodTypes = Periodtype.find();
    $scope.ok = function () {


    };
    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };


  }
)
