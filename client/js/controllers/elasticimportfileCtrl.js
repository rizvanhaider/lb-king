
angular.module('kingDS').controller('elasticimportfileCtrl', function ($scope, $uibModal, Notification, csvService, CSVContainner, mySocket) {

  $scope.isNoErrorinImport = true
  $scope.ImportErrorObjects = ""
  $scope.tabData = [
    {
      heading: 'New Template',
      route: 'mapping.newtmp'
    },
    {
      heading: 'Existing Template',
      route: 'mapping.existingtmp'
    }, {
      heading: 'Errors',
      route: 'mapping.errors'
    }
  ];


  // mySocket.emit('reqGetModels', null)
  // mySocket.on('resGetModels', function (models) {
  //   $scope.ALL_MODELS = models
  //   console.log(models)
  // })
  $scope.mappingTemplate = {}
  $scope.mappingTemplate.delimiter = ","
  $scope.mappingTemplate.noheader = false
  $scope.mappingTemplate.updateduplicate = false
  $scope.mappingTemplate.skipduplicate = false

  $scope.items = ['item1', 'item2', 'item3'];
  $scope.animationsEnabled = true;
  $scope.CSVdata = csvService.data

  $scope.updateduplicated = $scope.skipduplicated = false
  $scope.setprimarykey = function (update, skip, pkey) {
    $scope.mappingTemplate.updateduplicate = update
    $scope.mappingTemplate.skipduplicate = skip
    $scope.mappingTemplate.primarykey = pkey
    console.log(update, skip, pkey)
  }

  // $scope.getmodelproperties = function (model) {
  //   mySocket.emit('reqGetModelProperties', model)

  // }
  // mySocket.on('resGetModelProperties', function (data) {
  //   console.log(data)
  //   $scope.modelProperties = Object.keys(data)
  //   $scope.mappingTemplate["target"] = Object.keys(data)
  //   $scope.source = [];
  // })

  $scope.logdata = function (file) {
    console.log(file)
    //mySocket.emit('reqCSVHeaders', file)
    csvService.data.getcsvtojson(file)
    CSVContainner.getJSON({
      name: file
    }, function (data) {
      $scope.csvcolumns = data.data
      console.log("data from sdk: ", $scope.csvcolumns)
      // $scope.csvcolumnsonly = data.data
      // console.info($scope.mappingTemplate)
    })
  }
  $scope.startimport = function(file, catagory){

    CSVContainner.csvtoes({ name : file , catagory : catagory}, function (data) {
      console.log(data)
    })
  }
  //mySocket.on('resCSVHeaders', function(data){
  //  $scope.csvcolumnsonly = data
  //})

  $scope.open = function (size) {

    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'states/models/upload-csv.html',
      controller: 'csvUploadModelCtrl',
      size: size,
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });
  }

  $scope.opencsvsettings = function () {

    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'states/models/csv-settings.html',
      controller: 'csvsettingsCtrl',
      size: 'md',
      resolve: {
        mappingtemplate: function () {
          return $scope.mappingTemplate;
        }
      }
    });
  }


  $scope.sourceChanged = function (col, i) {
    $scope.source[i] = col
  }
  mySocket.emit('reqReadAllTemplates')
  mySocket.on('resReadAllTemplates', function (templates) {
    $scope.existingtemplates = templates
  })
  var createtemplate = $scope.createtemplate = function (file, model, templatename) {

    $scope.mappingTemplate["name"] = templatename
    $scope.mappingTemplate["nature"] = "csv"
    $scope.mappingTemplate["file"] = file
    $scope.mappingTemplate["model"] = model
    $scope.mappingTemplate["source"] = $scope.source
    console.log($scope.mappingTemplate)
    console.log($scope.source)
    mySocket.emit('reqCreateImportTemplate', $scope.mappingTemplate)
  }
  $scope.importwithSelected = function (template, model, csvfile) {
    if (template != undefined) {
      mySocket.emit('reqDoImportUsingTemplate', {
        template: template + ".json",
        model: model,
        csvfile: csvfile
      })
      Notification.info('Importing data from ' + csvfile);
    } else
      Notification.error("Please select template")
  }
  mySocket.on('resDoImportUsingTemplate', function (res) {
    //Notification.success('Successfully Imported! '+  res.total+ ", Rows Added: "+ res.totalsuccess);

  })

  mySocket.on('resErrorinImportUsingTemplate', function (res) {
    console.error(res.err)
    //$scope.isNoErrorinImport= false
    //$scope.ImportErrorObjects= $scope.ImportErrorObjects + res.err
    Notification.error("Error: ! " + res.err);
    //Notification.error('Error Description: '+  res.resp+ " Rows Added");
  })
  //mySocket.on('ErrorLog', function(log){
  //  $scope.importErrors = log
  //})
  $scope.importErrors = []
  mySocket.on('liveErrorsKingCSV', function (liveErrorsLingCSV) {
    $scope.importErrors.push(liveErrorsLingCSV.err)
    console.log(liveErrorsLingCSV.progress)
  })
  mySocket.on('importProgress', function (progress) {
    $scope.csvlog = progress
    $scope.impProgress = progress.percentage
    if (progress.total == progress.done)
      Notification.success('Successfully Imported! ' + progress.total + ", Rows ");
  })
  mySocket.on('invalidCSVTemplate', function () {
    Notification.error('Invalid CSV Template!');
  })
})
