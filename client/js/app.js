var APIPATH = "http://localhost:3000/api/";

var app = angular.module("kingDS", ['ui.router', 'ui.router.tabs', 'ui-notification', 'ui.bootstrap', 'angularFileUpload', 'btford.socket-io', 'ngResource', 'lbServices']);

app.factory('mySocket', function (socketFactory) {
  return socketFactory();
})

app.config(function ($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise('/home');

  $stateProvider

  // HOME STATES  ========================================
    .state('home', {
      url: '/home',
      templateUrl: 'states/home.html',
      controller: 'mainCtrl'
    })
    // DS STATES  ========================================
    .state('datasources', {
      url: '/datasources',
      templateUrl: 'states/datasources.html',
      controller: 'ConfDSCtrl'
    })

    // DS Models  ========================================
    .state('models', {
      url: '/models',
      templateUrl: 'states/models.html',
      controller: 'modelsCtrl'
    })

    // nested list with custom controller
    .state('datasources.config', {
      url: '/config',
      templateUrl: 'dsconfig.html',
      controller: function ($scope) {
        $scope.dogs = ['Bernese', 'Husky', 'Goldendoodle'];
      }
    })

    .state('mapping', {
      url: '/mapping',
      controller: 'mappingCtrl',
      templateUrl: 'states/mapping.html'
    })
    .state('mapping.newtmp', {
      url: '/mapping/newtmp',
      templateUrl: 'states/mapping/newtemplate.html'
    })
    .state('mapping.existingtmp', {
      url: '/mapping/existingtmp',
      templateUrl: 'states/mapping/existingtemplate.html'
    })
    .state('mapping.errors', {
      url: '/mapping/errors',
      templateUrl: 'states/mapping/errors.html'
    })


    .state('importapi', {
      url: '/importapi',
      templateUrl: 'states/importapi.html',
      controller: 'importAPICtrl'
    })
    .state('importunitdata', {
      url: '/importunitdata',
      templateUrl: 'states/importunitdata.html',
      controller: 'importunitdataCtrl'
    })

    .state('addDatasource', {
      url: '/addDatasource',
      templateUrl: 'states/addDatasource.html',
      controller: 'addDatasourceCtrl'
    })

    .state('schedular', {
      url: '/schedular',
      templateUrl: 'states/schedular.html'
    })
    .state('csvlog', {
      url: '/csvlog',
      templateUrl: 'states/csvlog.html',
      controller: 'logCtrl'
    })
    .state('elasticimport', {
      url: '/elasticimport',
      templateUrl: 'states/elasticimport.html',
      controller: 'elasticimportCtrl'

    })
    .state('elasticimport.api', {
      url: '/api',
      templateUrl: 'states/elasticimportapi.html',
      controller: 'elasticimportCtrl'
    })
    .state('elasticimport.csv', {
      url: '/csv',
      templateUrl: 'states/elasticimportfile.html',
      controller: 'elasticimportfileCtrl'
    });
});

app.controller("mainCtrl", function ($scope, $uibModal, csvService, CSVContainner, mySocket, Notification) {

  //$scope.tabData = [
  //  {
  //    heading: 'Core CSV',
  //    route: 'mapping.api'
  //  },
  //  {
  //    heading: 'Core API',
  //    route: 'mapping.csv'
  //  },
  //  {
  //    heading: 'CSV Template',
  //    route: 'mapping.csvtemplate'
  //  }
  //];
  //
  ///*******************TO UPDATE CLINT SIDE ANGULAR JS SDK****************************/
  $scope.updateAngularSDK = function () {
    Notification.info("Updating AngularJS SDK")
    mySocket.emit('reqUpdateAngularSDK')
  }
  mySocket.on('resUpdateAngularSDK', function (output) {
    Notification.success("AngularJS SDK updated!", output)
  })

  ///*******************TO UPDATE CLINT SIDE ANGULAR JS SDK****************************/
  //
  //
  //mySocket.emit('reqGetModels', null)
  //mySocket.on('resGetModels', function (models) {
  //  $scope.ALL_MODELS = models
  //})
  //$scope.mappingTemplate = {}
  //$scope.items = ['item1', 'item2', 'item3'];
  //$scope.animationsEnabled = true;
  //$scope.CSVdata = csvService.data
  //
  ////console.log(csvService.data)
  //
  //$scope.getmodelproperties = function (model) {
  //  mySocket.emit('reqGetModelProperties', model)
  //
  //}
  //mySocket.on('resGetModelProperties', function (data) {
  //  console.log(data)
  //  $scope.modelProperties = Object.keys(data)
  //  $scope.mappingTemplate["target"] = Object.keys(data)
  //  $scope.source = [];
  //})
  //
  //$scope.logdata = function (file) {
  //  console.log(file)
  //  //mySocket.emit('reqCSVHeaders', file)
  //  csvService.data.getcsvtojson(file)
  //  CSVContainner.getJSON({name: file}, function (data) {
  //    console.log("data from sdk: ", data)
  //    $scope.csvcolumnswithdata = data.data
  //    $scope.csvcolumnsonly = data.data
  //  })
  //}
  ////mySocket.on('resCSVHeaders', function(data){
  ////  $scope.csvcolumnsonly = data
  ////})
  //
  //$scope.open = function (size) {
  //
  //  var modalInstance = $uibModal.open({
  //    animation: $scope.animationsEnabled,
  //    templateUrl: 'states/models/upload-csv.html',
  //    controller: 'csvUploadModelCtrl',
  //    size: size,
  //    resolve: {
  //      items: function () {
  //        return $scope.items;
  //      }
  //    }
  //  });
  //}
  //
  //$scope.sourceChanged = function (col, i) {
  //  $scope.source[i] = col
  //}
  //mySocket.emit('reqReadAllTemplates')
  //mySocket.on('resReadAllTemplates', function (templates) {
  //  $scope.existingtemplates = templates
  //})
  //var createtemplate = $scope.createtemplate = function (file, model, templatename) {
  //
  //  $scope.mappingTemplate["name"] = templatename
  //  $scope.mappingTemplate["nature"] = "csv"
  //  $scope.mappingTemplate["file"] = file
  //  $scope.mappingTemplate["model"] = model
  //  $scope.mappingTemplate["source"] = $scope.source
  //  console.log($scope.mappingTemplate)
  //  console.log($scope.source)
  //  mySocket.emit('reqCreateImportTemplate', $scope.mappingTemplate)
  //}
  //$scope.importwithSelected = function (template, model) {
  //  mySocket.emit('reqDoImportUsingTemplate', {template: template + ".json", model: model})
  //}
})

app.controller('csvUploadModelCtrl', function ($scope, $uibModalInstance, items, csvService, FileUploader, $http) {

  var uploader = $scope.uploader = new FileUploader({
    scope: $scope, // to automatically update the html. Default: $rootScope
    //      url: 'http://localhost:3000/api/testmodel1s/dps/upload',
    url: APIPATH + 'CSVContainners/csvs/upload',
    formData: [
      {
        key: 'value'
      }
    ]
  });

  // ADDING FILTERS
  uploader.filters.push({
    name: 'filterName',
    fn: function (item, options) { // second user filter
      console.info('filter2');
      return true;
    }
  });

  // REGISTER HANDLERS
  // --------------------
  uploader.onAfterAddingFile = function (item) {
    console.info('After adding a file', item);
    item.file.name = (Math.round(Date.now())) + "-" + (Math.round(Math.random() * 1000)) + ".csv"
  };
  // --------------------
  uploader.onAfterAddingAll = function (items) {
    console.info('After adding all files', items);
  };
  // --------------------
  uploader.onWhenAddingFileFailed = function (item, filter, options) {
    console.info('When adding a file failed', item);
  };
  // --------------------
  uploader.onBeforeUploadItem = function (item) {
    console.info('Before upload', item);
  };
  // --------------------
  uploader.onProgressItem = function (item, progress) {
    console.info('Progress: ' + progress, item);
  };
  // --------------------
  uploader.onProgressAll = function (progress) {
    console.info('Total progress: ' + progress);
  };
  // --------------------
  uploader.onSuccessItem = function (item, response, status, headers) {
    console.info('Success', response, status, headers);
    $scope.$broadcast('uploadCompleted', item);
  };
  // --------------------
  uploader.onErrorItem = function (item, response, status, headers) {
    console.info('Error', response, status, headers);
  };
  // --------------------
  uploader.onCancelItem = function (item, response, status, headers) {
    console.info('Cancel', response, status);
  };
  // --------------------
  uploader.onCompleteItem = function (item, response, status, headers) {
    console.info('Complete', response, status, headers);
    csvService.data.responce = response
    csvService.data.csvfile = response.result.files.file[0].name
  };
  // --------------------
  uploader.onCompleteAll = function () {
    console.info('Complete all');
  };
  // --------------------
  //    }).controller('FilesController', function ($scope, $http) {

  $scope.importcsv = function (aFile, aHeaders) {

    var dataj = {
      "csv": aFile,
      "headerz": aHeaders
    }
    console.log(dataj);
//        $http.post(APIPATH +"orders/csvimport", dataj, {
    $http.post(APIPATH + "afps/csvimport", dataj, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }).then(function (res) {
      console.info(res);
    }, function (res) {
      console.error(res);
    });
  }

  $scope.load = function () {
    $http.get(APIPATH + 'Containers/csvs/files').success(function (data) {
      console.log(data);
      $scope.files = data;
    });
  };

  $scope.delete = function (index, id) {
    $http.delete(APIPATH + 'Containers/csvs/files' + encodeURIComponent(id)).success(function (data, status, headers) {
      $scope.files.splice(index, 1);
    });
  };

  $scope.$on('uploadCompleted', function (event) {
    console.log('uploadCompleted event received');
    $scope.load();
  });

  $scope.items = items;
  $scope.selected = {
    item: $scope.items[0]
  };
  $scope.ok = function () {
    //csvService.data.updatefile($scope.selected.item)
    $uibModalInstance.close($scope.selected.item);
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});


app.controller("modelsCtrl", function ($scope, mySocket, modelPropertiesService, $uibModal, $q) {
  mySocket.emit('reqInstalledDS', '')
  mySocket.on('resInstalledDS', function (res) {
    $scope.resInstalledDS = res;
  })
  $scope.getModelProperties = function (model) {
    modelPropertiesService.data.reqModelProp(model)
    $scope.showPropertiesModel('lg')
  }

  $scope.discoverDefinations = function (ds) {
    mySocket.emit('reqDSModels', ds)
    console.log("clicked")
  }
  mySocket.on('resDSModels', function (res) {
    $scope.resDSModels = res.data;
    console.log($scope.resDSModels)
  })

  $scope.createModel = function (table, ds) {
    console.log(table, ds)
    mySocket.emit('reqDiscoverAndBuildModel', {table: table, datasource: ds})
  }
  mySocket.on('resDiscoverAndBuildModel', function (res) {
    if (res.err) {
      console.error("Filed to create model")
    }
    else
      console.info(res.data, "Successfully created")

  })
  $scope.showPropertiesModel = function (size) {

    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'states/models/modelproperties.html',
      controller: 'modelPropCtrl',
      size: size

    });
  }


})

app.controller('modelPropCtrl', function ($scope, modelPropertiesService) {
  //$scope.properties= modelPropertiesService.data.modelproperties[Object.keys(modelPropertiesService.data.modelproperties)];
  //console.log($scope.properties)

  $scope.keys = Object.keys(modelPropertiesService.data.modelproperties)
  $scope.discoveredSchema = modelPropertiesService.data.modelproperties[$scope.keys[0]];
  $scope.modelProperties = Object.keys($scope.discoveredSchema.properties);
  console.log($scope.modelProperties)
})
//services
app.factory('csvService', function ($http) {
  var data = {
    csvfile: "upload file", responce: {}, csvdata: {}, updatefile: function (file) {
      data.csvfile = file
      console.log(file)
      console.log(data)
    }, getcsvtojson: function (file) {
      $http.get('http://localhost:3000/api/CSVContainners/getjson?name=' + file).then(function (data) {
        csvdata = data
        console.log(data)
      })
    }
  };


  return {
    data: data
  };
});

app.factory('modelPropertiesService', function (mySocket) {
  var data = {
    modelproperties: {}, reqModelProp: function (model) {
      mySocket.emit('reqModelProperties', model)
    }
  };
  mySocket.on('resModelProperties', function (res) {
    data.modelproperties = res.data
  })
  return {
    data: data
  };
});

