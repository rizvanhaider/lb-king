var fs = require("fs")
var rest = require('rest')
var request = require('request');
var dirTree = require('directory-tree')
var jsonfile = require('jsonfile')
var LOGFILES = './common/logs/'
var IDIMS_Templates = './common/idimscalls/'
var ES_Templates = './common/esmigrations/'
var app = require('../../server/server');
jsonfile.spaces = 4
module.exports = function (Commondir) {
    Commondir.readLOG = function (file, cb) {
        fs.readFile(LOGFILES + file, 'utf8', function (err, data) {
            if (err)
                cb(err, null)
            else
                cb(null, data)
        });
    }
    Commondir.remoteMethod(
        'readLOG', {
            http: {
                path: '/readLOG',
                verb: 'get'
            },
            accepts: {
                arg: 'name',
                type: 'string',
                http: {
                    source: 'query'
                }
            },
            returns: {
                arg: 'data',
                type: 'string'
            }
        }
    );

    //** Remote Method to Write IDIMS Call Template
    Commondir.createIDIMScallTemplate = function (obj, cb) {
        obj.options = {
                method: 'GET',
                url: obj.url,
               headers: {
                   authorization: 'Basic cGFrLWVvYy1kYXNoYm9hcmRAaWRpbXMtYXBpLnBrOnBAa0UwQyM4MHJkIQ==',
                    'x-dreamfactory-api-key': '1864d6be0fd8a671c6e3a43ef9d77ad07ae48f1f1d4694c5e8fd49aea201a053'
                }
            }
        var file = IDIMS_Templates + obj.tablename + ".json"
            //        console.log(obj)
        jsonfile.writeFile(file, obj, function (err) {
            if (!err) {
                if (obj.addDataElement) {
                    addDataElements(obj.datasetid, obj.apiProperties, function (err, resp) {
                        console.log(err, " : ", resp)
                    })
                } else
                    console.log("file Written without creating dataelements")
                cb(err, "File Written Successfully")
            } else
                console.error(err)
        })
    }
    Commondir.remoteMethod(
        'createIDIMScallTemplate', {
            http: {
                path: '/createIDIMScallTemplate',
                verb: 'post'
            },
            accepts: {
                arg: 'data',
                type: 'object'
            },
            returns: {
                arg: 'data',
                type: 'string'
            }
        }
    );

    //** Remote Method to Write elastic search Call Template
    Commondir.createElasticTemplate = function (obj, cb) {

        var file = ES_Templates + obj.campaign + '-'+ obj.type + ".json"
            //        console.log(obj)
        jsonfile.writeFile(file, obj, function (err) {
            if (!err) {
                if (obj.addDataElement) {
                    addDataElements(obj.datasetid, obj.apiProperties, function (err, resp) {
                        console.log(err, " : ", resp)
                    })
                } else
                    console.log("file Written without creating dataelements")
                cb(err, "File Written Successfully")
            } else
                console.error(err)
        })
    }
    Commondir.remoteMethod(
        'createElasticTemplate', {
            http: {
                path: '/createElasticTemplate',
                verb: 'post'
            },
            accepts: {
                arg: 'data',
                type: 'object'
            },
            returns: {
                arg: 'data',
                type: 'string'
            }
        }
    );

    //** Remote Method to Delete IDIMS Call Template
    Commondir.deleteIDIMScallTemplate = function (path, cb) {
        var file = IDIMS_Templates + path + ".json"
        console.log(path)
        fs.unlinkSync(file);
        cb(null, path + " deleted")

    }
    Commondir.remoteMethod(
        'deleteIDIMScallTemplate', {
            http: {
                path: '/deleteIDIMScallTemplate',
                verb: 'delete'
            },
            accepts: {
                arg: 'filename',
                type: 'string'
            },
            returns: {
                arg: 'data',
                type: 'string'
            }
        }
    );

    //** Remote Method to Read IDIMS Call Templates
    Commondir.IDIMScallTemplates = function (cb) {
        var tree = dirTree(IDIMS_Templates, ['.json']);
        cb(null, tree)
    }
    Commondir.remoteMethod(
        'IDIMScallTemplates', {
            http: {
                path: '/IDIMScallTemplates',
                verb: 'get'
            },
            returns: {
                arg: 'data',
                type: 'string'
            }
        }
    );
    //** Remote Method to Read ES Call Templates
    Commondir.getEScalls = function (cb) {
        var tree = dirTree(ES_Templates, ['.json']);
        cb(null, tree)
    }
    Commondir.remoteMethod(
        'getEScalls', {
            http: {
                path: '/getEScalls',
                verb: 'get'
            },
            returns: {
                arg: 'data',
                type: 'string'
            }
        }
    );

    //** Remote Method to Read IDIMS Call Template
    Commondir.IDIMScallTemplate = function (path, cb) {
        jsonfile.readFile(IDIMS_Templates + path, function (err, data) {
            //            console.log(data)
            cb(err, data)
        })
    }
    Commondir.remoteMethod(
        'IDIMScallTemplate', {
            http: {
                path: '/IDIMScallTemplate',
                verb: 'get'
            },
            accepts: {
                arg: 'path',
                type: 'string'
            },
            returns: {
                arg: 'data',
                type: 'object'
            }
        }
    );
    //** Remote Method to Read ES Call JSON DATA
    Commondir.getEScallsData = function (path, cb) {
        jsonfile.readFile(ES_Templates + path, function (err, data) {
            // console.log()
            cb(err, data)
        })
    }
    Commondir.remoteMethod(
        'getEScallsData', {
            http: {
                path: '/getEScallsData',
                verb: 'get'
            },
            accepts: {
                arg: 'path',
                type: 'string'
            },
            returns: {
                arg: 'data',
                type: 'object'
            }
        }
    );

    //** Remote Method to Get  IDIMS API Propery keys in an array
    Commondir.getIDIMSkeys = function (url, type, cb) {
        console.log("Call parms: ", url, type)
        if (type === 'IDIMS') {

            rest(url).then(function (response) {
                var data = JSON.parse(response.entity).data.data[0];

                var keys = JSON.parse(response.entity).data.keys;
                var keyswithdatatype = []
                var keyslength = keys.length;
                //            console.log("DATA: ", data)
                //            console.log("Keys: ", keys)
                keys.forEach(function (key) {

                        if (!isNaN(parseFloat(data[key])) && isFinite(data[key])) {
                            keyswithdatatype.push({
                                "key": key,
                                datatype: "number"
                            })
                        } else
                            keyswithdatatype.push({
                                "key": key,
                                datatype: "string"
                            })

//                        console.log("obj: ", keyswithdatatype)
                        if (--keyslength == 0)
                            cb(null, keyswithdatatype)
                    })
                    //        cb(null, keys)
            }, function (err) {
                cb(err, [])
            })

        } else if (type === 'LQAS') {
            var options = {
                method: 'GET',
                url: url,
               headers: {
                   authorization: 'Basic cGFrLWVvYy1kYXNoYm9hcmRAaWRpbXMtYXBpLnBrOnBAa0UwQyM4MHJkIQ==',
                    'x-dreamfactory-api-key': '1864d6be0fd8a671c6e3a43ef9d77ad07ae48f1f1d4694c5e8fd49aea201a053'
                }
            };

            request(options, function (error, response, body) {
                if (error) throw new Error(error);
                var data = JSON.parse(body).resource[0]
                var keys = Object.keys(data);
                 var keyswithdatatype = []
                var keyslength = keys.length;
                console.log(data);
                keys.forEach(function (key) {

                        if (!isNaN(parseFloat(data[key])) && isFinite(data[key])) {
                            keyswithdatatype.push({
                                "key": key,
                                datatype: "number"
                            })
                        } else
                            keyswithdatatype.push({
                                "key": key,
                                datatype: "string"
                            })

//                        console.log("obj: ", keyswithdatatype)
                        if (--keyslength == 0)
                            cb(null, keyswithdatatype)
                    })
                if(error){
                    cb(err, null)
                }
            });

        }
    }
    Commondir.remoteMethod(
        'getIDIMSkeys', {
            http: {
                path: '/getIDMSkeys',
                verb: 'get'
            },
            accepts: [{
                arg: 'data',
                type: 'string'
            }, {
                arg: 'type',
                type: 'string'
            }],
            returns: {
                arg: 'keys',
                type: 'array'
            }
        }
    );


    //functions
    var addDataElements = function (aDatasetID, keys, callback) {
        var dataSource = app.dataSources.MES;
        var modelDataelement = dataSource.models['Dataelement']


        var dataElementIDs = []
        var keysize = keys.length
        keys.forEach(function (key) {
            keysize--
            modelDataelement.create({
                "name": key.key,
                "shortname": key.key,
                "description": key.key,
                "valuetype": key.datatype,
                "aggregationtype": "string",
                "parentid": 0,
                "disaggregationCombinationid": 0,
                "url": "",
                "domaintype": "AGGRIGATED",
                "lastupdated": new Date().toUTCString(),
                "zeroissignificant": true,
                "formname": "string",
                "uid": "-1",
                "optionsetid": 0,
                "legendsetid": 0,
                "created": new Date().toUTCString(),
                "userid": 0,
                "publicaccess": "string",
                "datasetid": aDatasetID,
                "commentoptionsetid": 0
            }, function (err, responce) {
                if (!err) {
                    modelDataelement.findOne({
                        where: {
                            name: key.key,
                            datasetid: aDatasetID
                        }
                    }, function (err, resp) {
                        dataElementIDs.push(resp.dataelementid)
                        addDatasetmembers(dataSource, aDatasetID, resp.dataelementid, callback)
                        if (keysize === 0)
                            callback(err, keys.length)
                        console.log(dataElementIDs, "END^^^")
                    })
                } else {
                    console.log("Err: ", err)
                    callback(err, dataElementIDs)
                }
            })
            console.log(key, 'line:87')
        })
    }

    function addDatasetmembers(dataSource, aDatasetID, aDataelementID, callback) {
        var modelDatasetmembers = dataSource.models['Datasetmembers']
        modelDatasetmembers.create({
            "datasetid": aDatasetID,
            "dataelementid": aDataelementID
        }, function (err, responce) {
            //      if(err)
            callback(err, responce)
                //      else
                //        callback(null, responce)
        })
    }

};
