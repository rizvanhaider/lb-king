var app = require('../../server/server');
var fs = require("fs")
var request = require('request');
var JSONPATH = './csvcontainer/jsons/'
var dataSource = app.dataSources['APIDS'];
var kingimport = require('../../king_modules/lbdsoperations')
var kingcsv = require('../../king_modules/kingcsv')
var ModelBuilder = require('../../node_modules/loopback-datasource-juggler').ModelBuilder;
var builder = new ModelBuilder();


module.exports = function (JsonContainer) {
    JsonContainer.json_to_db = function (file, tablename, cb) {

        var options = {
            method: 'GET',
            url: 'http://localhost:3000/api/jsoncontainers/jsons/download/'+ file
        };

        request(options, function (error, response, body) {
            //            if (error) throw new Error(error);
            var data = JSON.parse(body)
            var keys = Object.keys(data[0])
            var modelDef = {};
            for (var i = 0; i < keys.length; i++) {

                modelDef[keys[i]] = 'String'
            }

            var modelone = builder.define(tablename, modelDef);
            modelone.attachTo(dataSource);

            dataSource.autoupdate(tablename, function (err) {
                    if (err) {
                        console.log(err)
                    } else {
                        modelone.create(data, function (err, resp) {
                                console.log("data posted")
                            })
                            //                    var model = app.models
                            //                    console.log("csv model: ", model)
                        cb(null, data.length);
                    }
                })
                //            cb(error, JSON.parse(body));
        });

    }
    JsonContainer.remoteMethod(
        'json_to_db', {
            http: {
                path: '/json_to_db',
                verb: 'get'
            },
            accepts: [{
                arg: 'name',
                type: 'string',
                http: {
                    source: 'query'
                }
            }, {
                arg: 'table',
                type: 'string',
                http: {
                    source: 'query'
                }
            }],
            returns: {
                arg: 'data',
                type: 'string'
            }
        }
    );
};
