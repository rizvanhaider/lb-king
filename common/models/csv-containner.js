var Converter = require("csvtojson").Converter;
var app = require('../../server/server');
var fs = require("fs")
var CSVPATH = './csvcontainer/csvs/'
var dataSource = app.dataSources['APIDS'];
var kingimport = require('../../king_modules/lbdsoperations')

var xpElasticETL = require('../../king_modules/xpElasticETL')
var kingcsv = require('../../king_modules/kingcsv')
var ModelBuilder = require('../../node_modules/loopback-datasource-juggler').ModelBuilder;
var builder = new ModelBuilder();


module.exports = function (CsvContainner) {
    CsvContainner.getJSON = function (file, cb) {
        CsvContainner.getFile('csvs', file, function (err, data) {
            if (!err) {
                var converter = new Converter({});
                converter.on("error", function (err) {
                    cb(err, err)
                })
                converter.on("record_parsed", function (resultRow, rawRow, rowIndex) {
                    if (rowIndex > 0)
                        return
                    console.log(Object.keys(resultRow))
                    cb(null, Object.keys(resultRow));
                });

                fs.createReadStream(CSVPATH + file).pipe(converter);
            } else
                cb(err, null)
        });
    }
    CsvContainner.remoteMethod(
        'getJSON', {
            http: {
                path: '/getjson',
                verb: 'get'
            },
            accepts: {
                arg: 'name',
                type: 'string',
                http: {
                    source: 'query'
                }
            },
            returns: {
                arg: 'data',
                type: 'string'
            }
        }
    );

    CsvContainner.readCSV = function (file, cb) {
        kingcsv.readCSVfile(file, function (err, jsonArray) {
            cb(err, jsonArray)
        })

    }
    CsvContainner.remoteMethod(
        'readCSV', {
            http: {
                path: '/readCSV',
                verb: 'get'
            },
            accepts: {
                arg: 'name',
                type: 'string',
                http: {
                    source: 'query'
                }
            },
            returns: {
                arg: 'data',
                type: 'string'
            }
        }
    );

    CsvContainner.csvtoes = function (file, cat, cb) {
        kingcsv.readCSVfile(file, function (err, jsonArray) {
            xpElasticETL.csvtoes(jsonArray, cat)
            cb(err, jsonArray)
        })

    }
    CsvContainner.remoteMethod(
        'csvtoes', {
            http: {
                path: '/csvtoes',
                verb: 'get'
            },
            accepts: [{
                arg: 'name',
                type: 'string',
                http: {
                    source: 'query'
                }
            },{
                arg: 'catagory',
                type: 'string',
                http: {
                    source: 'query'
                }
            }],
            returns: {
                arg: 'data',
                type: 'string'
            }
        }
    );

    CsvContainner.csvtodb = function (file, cb) {

        //        console.log(dataSource)

        kingcsv.readCSVfile(file, function (err, jsonArray) {

            var keys = Object.keys(jsonArray[0])
            var modelDef = {};
            for (var i = 0; i < keys.length; i++) {
                if (keys[i] != 'ID')
                    modelDef[keys[i]] = 'String'
            }

            var modelone = builder.define('tmpafp', modelDef);
            modelone.attachTo(dataSource);

            dataSource.autoupdate('tmpafp', function (err) {
                if (err) {
                    console.log(err)
                } else {
                    modelone.create(jsonArray, function(err, resp){
                        console.log("data posted")
                    })
//                    var model = app.models
//                    console.log("csv model: ", model)
                    cb(null, jsonArray.length);
                }
            })
        })




    }
    CsvContainner.remoteMethod(
        'csvtodb', {
            http: {
                path: '/csvtodb',
                verb: 'get'
            },
            accepts: {
                arg: 'name',
                type: 'string',
                http: {
                    source: 'query'
                }
            },
            returns: {
                arg: 'data',
                type: 'string'
            }
        }
    );
    CsvContainner.unitDataLocationMapping = function (file, cb) {
        CsvContainner.getFile('csvs', file, function (err, data) {
            if (!err) {

                var EocAfpUcTbl = app.models.EocAfpUcTbl;
                var Location = app.models.Location;
                var Trackedentityinstance = app.models.Trackedentityinstance;
                var Trackedentityattributevalue = app.models.Trackedentityattributevalue;
                var Trackedentityattribute = app.models.Trackedentityattribute;
                var Trackedentitydatavalue = app.models.Trackedentitydatavalue;
                var Programinstance = app.models.Programinstance;
                var Programstageinstance = app.models.Programstageinstance;
                var Programstagedataelement = app.models.Programstagedataelement;
                var Dataelement = app.models.Dataelement;

                //                Dataelement.find({where : {domaintype : 'TRACKER'}},function(err, deresp){
                //
                //                })
                //            var app= CsvContainner.app
                //                console.log("app: ", app)
                var converter = new Converter({});
                converter.on("error", function (err) {
                    cb(err, err)
                })
                converter.on("end_parsed", function (jsonArray) {
                    var AFPKEYS = Object.keys(jsonArray[0])
                    jsonArray.forEach(function (filerow) {

                        //                        console.log(filerow.epid)
                        EocAfpUcTbl.findOne({
                            where: {
                                epid: filerow.epid
                            }
                        }, function (err, success) {
                            //                            console.log("epid: ", filerow.epid, "location: ", success)
                            if (success) {
                                Location.findOne({
                                    where: {
                                        locationcode: success.ucode
                                    }
                                }, function (err, resp) {

                                    Programinstance.create({
                                        "programid": 1,
                                        "locationid": resp.locationid
                                    }, function (err, presp) {
                                        Programstageinstance.create({
                                            "programinstanceid": presp.programinstanceid,
                                            "programstageid": 1,
                                            "locationid": presp.locationid
                                        }, function (err, psiresp) {
                                            AFPKEYS.forEach(function (col) {
                                                Dataelement.findOne({
                                                    where: {
                                                        name: col,
                                                        domaintype: 'TRACKER'
                                                    }
                                                }, function (err, deresp) {
                                                    if (!err && deresp) {
                                                        Trackedentitydatavalue.create({
                                                            "programstageinstanceid": psiresp.programstageinstanceid,
                                                            "dataelementid": deresp.dataelementid,
                                                            "value": filerow[col]
                                                        }, function (err, cb) {
                                                            if (err) {
                                                                console.log("Error! ", err)
                                                            }
                                                            //                                                            else console.log(cb)
                                                        })
                                                    }
                                                })
                                            })
                                            if (err)
                                                console.log("Err in psi: ", err)
                                            else
                                                console.log("presp: ", psiresp)
                                        })
                                    })


                                    Trackedentityinstance.create({
                                        "locationid": resp.locationid,
                                        "trackedentityid": 1
                                    }, function (err, tresp) {
                                        //                                        console.log("trackedentityinstance", tresp)
                                        AFPKEYS.forEach(function (col) {
                                            Trackedentityattribute.findOne({
                                                where: {
                                                    name: col
                                                }
                                            }, function (err, atresp) {
                                                if (!err && atresp) {
                                                    //                                     console.log("atribid ", atresp)

                                                    Trackedentityattributevalue.create({
                                                        "trackedentityinstanceid": tresp.trackedentityinstanceid,
                                                        "trackedentityattributeid": atresp.trackedentityattributeid,
                                                        "value": filerow[col]
                                                    }, function (err, resp) {
                                                        if (err)
                                                            console.log("err: ", err)
                                                        else
                                                            console.log("resp: ", resp)
                                                    })

                                                }
                                            })
                                        })

                                    });

                                    console.log("locationid: ", resp.locationid, resp.locationcode, resp.name)
                                })
                            } else
                                console.log(filerow.epid, " is invalid id")
                        })
                    })
                    cb(null, jsonArray);
                });
                fs.createReadStream(CSVPATH + file).pipe(converter);
            } else
                cb(err, null)
        });
    }
    CsvContainner.remoteMethod(
        'unitDataLocationMapping', {
            http: {
                path: '/unitDataLocationMapping',
                verb: 'post'
            },
            accepts: {
                arg: 'name',
                type: 'string',
                http: {
                    source: 'query'
                }
            },
            returns: {
                arg: 'data',
                type: 'string'
            }
        }
    );
};
