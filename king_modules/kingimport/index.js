var TEMPLATE_PATH = "./common/templates/"
var QUERY_PATH = "./common/queries/idimsToDatavalue.json"
var LOG_PATH = "./common/logs/"
var CSV_PATH = "./csvcontainer/csvs/";
var jsonfile = require('jsonfile')
var rest = require('rest')
var request = require('request');
jsonfile.spaces = 4;
var Converter = require("csvtojson").Converter;
var dirTree = require('directory-tree');
var _eval = require('eval')
var winston = require('winston');
var pg = require('pg');
var conString = "postgres://postgres:root@localhost/RE";
var API_Templates = './common/idimscalls/'

var ModelBuilder = require('../../node_modules/loopback-datasource-juggler').ModelBuilder;
var builder = new ModelBuilder();

//winston.add(winston.transports.File, { filename: './common/logs/'+csvfile+'.log', exitOnError: false });

var DoImportUsingTemplate = function (file, model, csvfile, kingApp, callback) {

  //winston.add(winston.transports.File, { filename: './common/logs/'+csvfile+'.log' });
  //winston.remove(winston.transports.Console);


  var date = new Date();

  var logger = new (winston.Logger)({
    transports: [
      new winston.transports.File({filename: './common/logs/' + csvfile + '.log', json: true, exitOnError: false})
    ],
    exceptionHandlers: [
      new winston.transports.File({filename: './common/logs/' + csvfile + '.log', json: true, exitOnError: false})
    ]
  });


  var dataSource = kingApp.dataSources.MES;
  var dataSourceCSV = kingApp.dataSources.container;
  var targetModel = dataSource.models[model]
  var csvContainer = dataSourceCSV.models["CSVContainner"]
  var converter = new Converter({});


  var postObject = "var evalObj =  { "

  jsonfile.readFile(TEMPLATE_PATH + file, function (err, obj) {


    //***************CSV FILE VALIDATION*********************//
    var validationconverter = new Converter({delimiter: obj.delimiter, noheader: obj.noheader});
    //validationconverter.on("error", function(err){
    //  console.log("Error reading CSV "+ csvfile)
    //  //cb(err, err)
    //})
    validationconverter.on("record_parsed", function (resultRow, rawRow, rowIndex) {
      if (rowIndex > 0)
        return
      var isCSVValid = !superbag(Object.keys(resultRow), obj.source)
      console.log("superbag: ", isCSVValid)

    })
    //callback(1, "invalid csv Template")

    //  //cb(null, Object.keys(resultRow));
    //});
    //
    require('fs').createReadStream(CSV_PATH + csvfile).pipe(validationconverter);

    //******************END CSV FILE VALIDATION*********************//
    //  superbag()

    if (isCSVValid) {
      var updateduplicate = obj.updateduplicate
      var skipduplicate = obj.skipduplicate
      var primarykey = obj.primarykey;
      var converter = new Converter({delimiter: obj.delimiter, noheader: obj.noheader});
      converter.on("error", function (err) {
        callback(err, "Error on reading csv file")
      })

      converter.on("end_parsed", function (jsonArray) {
        //            console.log(jsonArray); //here is your result jsonarray
        for (var i = 0; i < obj.target.length; i++) {
          if (i == obj.target.length - 1)
            postObject = postObject + '"' + obj.target[i] + '"' + ' : row.' + obj.source[i] + ' }'
          else
            postObject = postObject + '"' + obj.target[i] + '"' + ' : row.' + obj.source[i] + ' , '
//                console.log(postObject)
        }
        //            console.log("postObject" , postObject)
        var ImportErrors = []
        var progress = {
          "total": jsonArray.length,
          "done": 0,
          "percentage": 0,
          "deleted": 0,
          "processed": 0,
          "remaining": jsonArray.length,
          "errors": 0
        }
        var collength = jsonArray.length
        var coldone = jsonArray.length;
        var duplicateCounter = 0
        jsonArray.forEach(function (row) {
          targetModel.destroyAll({id: row[primarykey]}, function (err, responce) {
            //console.log("Deleted: ", responce)
            duplicateCounter++
            if (responce.count === 1)
              progress.deleted++
            if (jsonArray.length === duplicateCounter) {
              importrowstotable()
            }
          })
        })
        function importrowstotable() {

          jsonArray.forEach(function (row) {
            eval(postObject)
//                                    console.log(evalObj)

            targetModel.create(evalObj, function (err, resp) {
              progress.processed++
              progress.percentage = ((progress.done * 100) / progress.total).toFixed(0);
              if (err) {
                progress.errors++
                logger.log('error', err)
                //logger.log('error', err)
                console.log("KCSV Error: ", err)
                ImportErrors.push(err)
                kingApp.io.emit('liveErrorsKingCSV', {err: err.message, progress: progress})
                //callback(err, collength)
              }
              else {
                logger.log('info', resp)
                progress.remaining--
                progress.done++
                kingApp.io.emit('importProgress', progress)
              }
              if (progress.percentage % 1 == 0) {
              }
              if (progress.processed === jsonArray.length) {
                callback(ImportErrors, {progress: progress, logpath: "./common/logs/" + csvfile + ".log"})
              }

            })
          })
        }

        //callback(ImportErrors, {progress: progress,  logpath : "./common/logs/"+ csvfile +".log"})

        //
//            callback(null, jsonArray)
        //            callback(null, obj.source)
      });

      //read from file
      require("fs").createReadStream(CSV_PATH + csvfile).pipe(converter);
//        require("fs").createReadStream(CSV_PATH + obj.file).pipe(converter);

    }
  })
}
function superbag(sup, sub) {
  sup.sort();
  sub.sort();
  var i, j;
  for (i = 0, j = 0; i < sup.length && j < sub.length;) {
    if (sup[i] < sub[j]) {
      ++i;
    } else if (sup[i] == sub[j]) {
      ++i;
      ++j;
    } else {
      // sub[j] not in sup, so sub not subbag
      return false;
    }
  }
  // make sure there are no elements left in sub
  return j == sub.length;
}

var createTemplate = function (template, callback) {
  var file = TEMPLATE_PATH + template.name + ".json"
  jsonfile.writeFile(file, template, function (err) {
    if (err)
      callback(err, null)
    else
      callback(null, file)
  })
}
var readAllTemplates = function (callback) {
  var templateTree = dirTree.directoryTree(TEMPLATE_PATH, ['.json']);
  callback(null, templateTree.children)
}

var addToDataValue = function (datasetid, tablename, period, query, delquery,  callback) {

  var time_start_delete = new Date(); // time when rest get data
  pg.connect(conString, function (err, client, done) {
    if (err) {
      return console.error('error fetching client from pool', err);
    }
    //console.log("Query: ", query)
    client.query(delquery, function (err, result) {
      //call `done()` to release the client back to the pool
      done();

      if (err) {
        return console.error('error running query', err);
      }
      //callback(err, result.rows)
      var delRows = result.rows.count
      console.log("Delete Query: ", result.rows);
      var time_end_delete_start_insert = new Date();
      //output: 1

      client.query(query, function (err, result) {
        //call `done()` to release the client back to the pool
        done();

        if (err) {
          return console.error('error running query', err);
        }
        var insRows = result.rows.count
        console.log("Insert Query: ", result.rows);
        var time_end_insert = new Date();
        var time_for_delete = time_end_delete_start_insert - time_start_delete
        var time_for_insert = time_end_insert - time_end_delete_start_insert
        callback(err, {"insRows": result.rows, "delrows": delRows,"delTime": time_for_delete, "insTime": time_for_insert})
        //output: 2
      });

    });
  });
  //})
}

var importIDIMSAPIData = function (app, datasetid, url, tablename, period, calltype, callback) {

  var query = null
  var delquery = null
  jsonfile.readFile(QUERY_PATH, function (err, obj) {
    if (err) {
      console.error(err)
    } else {
      delquery = obj['' + datasetid].delquery.replace(/QTABLENAME/g, tablename).replace(/QPERIOD/g, period)
      query = obj['' + datasetid].query.replace(/QTABLENAME/g, tablename).replace(/QPERIOD/g, period)
      //console.log(query)
    }
  })
  if (calltype == 'LQAS') {
    var options = {
      method: 'GET',
      url: url,
      headers: {
        authorization: 'Basic cGFrLWVvYy1kYXNoYm9hcmRAaWRpbXMtYXBpLnBrOnBAa0UwQyM4MHJkIQ==',
        'x-dreamfactory-api-key': '1864d6be0fd8a671c6e3a43ef9d77ad07ae48f1f1d4694c5e8fd49aea201a053'
      }
    };
  } else {
    var options = {
      method: 'GET',
      url: url
    };
  }

  var dataSource = app.dataSources.APIDS;
  var file = API_Templates + tablename + ".json"
  var date1 = new Date(); // time when rest get data
  console.log("calling .. :" + url);
  request(options, function (error, response, body) {
    if (error) throw new Error(error);
    //rest(url).then(function (response) {

    //if (response.status.code != 200) {
    //  app.io.emit("APIError", "Error while accessing the API, Error Code: " + response.status.code)
    //
    //}
    //    console.log(body)
    if (calltype == 'LQAS') {
      var data = JSON.parse(body).resource
      var respData = JSON.parse(body).resource
      var fobj = JSON.parse(body).resource[0]
      var keys = Object.keys(fobj);
    }
    else {
      var data = JSON.parse(body);

      var keys = data.data.keys;
      var respData = data.data.data;
    }

    //app.io.emit("datakeys", keys);

    //Model Defination
    var modelDef = {};
    for (var i = 0; i < keys.length; i++) {
      if (keys[i] != 'ID')
        modelDef[keys[i]] = 'String'
    }


    /**************************/
    var modelone = builder.define(tablename, modelDef);
    modelone.attachTo(dataSource);
    /**************************/
    //        console.log(modelDef);

    //Creating table against model in database
    //                var modelone = dataSource.createModel('Modelone', modelDef);
    dataSource.automigrate(tablename, function (err) {
      if (err) throw err;
      var count = respData.length;
      respData.forEach(function (dataobj) {
        dataSource.models[tablename].create(dataobj, function (err, model) {
          //if (err) throw err;
          process.stdout.write("Remaining objects: " + count + " \r");
          //console.log(count);
          count--;
          if (count === 0) {
            var date2 = new Date(); // time when rest get data
            var timetaken = (date2 - date1) / 1000


            //console.log( timetaken+ "seconds");
            callback({
              "url": url,
              "calltype": calltype,
              "tableName":  tablename,
              "error": "" + err,
              "description": tablename + " updated in " + timetaken + " seconds with " + respData.length + " rows via lib-king import module",
              "apiLength": respData.length,
              "lastMigration": new Date(),
              "columnsMigrated": respData.length,
              "timeTakenSec": timetaken,
              "query": query,
              "delquery": delquery,
              "id": 0
            })
          }
        });
      });
      console.log("model created!")
    });
  });
}

var kingimport = {
  DoImportUsingTemplate: DoImportUsingTemplate,
  readAllTemplates: readAllTemplates,
  createTemplate: createTemplate,
  importIDIMSAPIData: importIDIMSAPIData,
  addToDataValue: addToDataValue
}

module.exports = kingimport
