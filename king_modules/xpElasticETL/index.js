var elasticsearch = require('elasticsearch');
var jsonfile = require('jsonfile')
var forEach = require('async-foreach').forEach;
var request = require("request");
var _ = require('lodash');
var ProgressBar = require('progress');
var winston = require('winston');
var Elasticsearch = require('winston-elasticsearch');

var esTransportOpts = {
    index: 'winstonlog',
    client: new elasticsearch.Client({
        host: '58.65.136.109:9200'
    })
};
// winston.add(winston.transports.Elasticsearch, esTransportOpts);
var logger = new winston.Logger({
    transports: [
        new Elasticsearch(esTransportOpts)
    ]
});


jsonfile.spaces = 4

var es_client = new elasticsearch.Client({
    host: '58.65.136.109:9200'
        //            log: 'trace'
});

var dashboards = function (callback) {
    es_client.search({
        index: 'etltest',
        type: 'dymmy'
    }).then(function (resp) {
        var hits = resp.hits.hits;

        callback(resp.hits.hits)
    }, function (err) {
        console.trace(err.message);
    });
}
var addESCron = function (esCron, callback) {
    callback(esCron)
}

// core function to migrate data from API to Elasticsearch
var exeIDIMStoES = function (api_meta) {
    console.log(api_meta)
        // logger.log({
        //     "message": "importing " + api_meta.catagory,
        //     "level": "info"
        //     }
        // )
    var startTime = new Date();
    console.log(new Date(), " | w calling api..")
    logger.info(api_meta.type + ' importing started', {
        "template": api_meta,
        "message": "importing " + api_meta.catagory,
        "level": "info"
    })

    var bar = new ProgressBar(':bar', {
        total: 100
    });

    if (api_meta.catagory == 'LQAS') {
        var options = {
            method: 'GET',
            url: api_meta.url,
            headers: {
                authorization: 'Basic cGFrLWVvYy1kYXNoYm9hcmRAaWRpbXMtYXBpLnBrOnBAa0UwQyM4MHJkIQ==',
                'x-dreamfactory-api-key': '1864d6be0fd8a671c6e3a43ef9d77ad07ae48f1f1d4694c5e8fd49aea201a053'
            }
        };
    } else {
        var options = {
            method: 'GET',
            url: api_meta.url
        };
    }
    request(options, function (error, response, body) {
        console.log(new Date(), " | call done ..")

        if (!error && response.statusCode === 200) {


            if (api_meta.catagory == 'LQAS') {
                // var data = JSON.parse(body).resource
                var rest_resp = JSON.parse(body).resource
                    // var fobj = JSON.parse(body).resource[0]
                    // var keys = Object.keys(fobj);
            } else {
                var rest_resp = JSON.parse(body).data.data
                    // var data = JSON.parse(body);

                // var keys = data.data.keys;
                // var respData = data.data.data;
            }


            var bar = new ProgressBar(':bar', {
                total: rest_resp.length
            });
            var TOTAL_OBJECTS = rest_resp.length
            var TOTAL_SUCCESS = 0
            var TOTAL_TREVERSED = 0
            var error_Detail = []


            forEach(rest_resp, function (item, index) {
                TOTAL_TREVERSED++
                var done = this.async();
                addLocationData(rename(item, api_meta.prefix, api_meta), api_meta.prefix, false, function (err, obj) {
                    done()
                    if (!err && obj) {
                        //                        console.log("obj _ ready to push: ", obj)
                        //                        console.log("err __ ready to push: ", obj)
                        obj.campaign = api_meta.campaign
                        pushToES(api_meta.type, item[api_meta.prefix + 'ID'], obj, function (err, resp) {
                            //                            done()
                            bar.tick();
                            if (err)
                                console.log("Error in pushing obj: ", err)
                            else
                                TOTAL_SUCCESS++

                                if (index == TOTAL_OBJECTS - 1) {
                                    console.log("Migration Done! TOTAL: ", TOTAL_OBJECTS, " SUCCESS: ", TOTAL_SUCCESS, " TREVERSED", TOTAL_TREVERSED)
                                    logger.info(api_meta.type + ' importing end', {
                                        "template": api_meta,
                                        "total_records": TOTAL_OBJECTS,
                                        "total_success": TOTAL_SUCCESS,
                                        "total_treversed": TOTAL_TREVERSED,
                                        "total_faild": TOTAL_OBJECTS - TOTAL_SUCCESS,
                                        "time_taken": new Date() - startTime,
                                        "errors": error_Detail,
                                        "level": "info"
                                    })

                                }
                        })
                    } else {

                        error_Detail.push(err)
                        console.log("locErr: ", err)
                    }
                })

            })
        }
    })
}

var parseDataTypes = function (api_meta, data, callback) {
        //not in use currentyly
        if (api_meta.hasOwnProperty('apiProperties')) {
            forEach(api_meta.apiProperties, function (item, index) {
                if (item.datatype === "number") {
                    console.log("beforeparse: ", data)
                    data[api_meta.type + item.datatype] = parseInt(data[api_meta.type + item.datatype + item.datatype]);
                }
            }, callback("optimized", data))
        } else {
            callback("asis", data)
        }
    }
    // pushToES, function to create/update data in elasticsearch against type and id for data(body: data to be saved or replaced) as a third parameter.
var pushToES = function (_type, _id, _body, callback) {
    //    console.log("_body : ", _body)
    es_client.index({
        index: 'etl',
        type: _type,
        id: _id,
        body: _body
    }, function (error, response) {
        if (error) {
            console.log("######## PUSHTOES ERROR ########: ", _type, _id, _body)
                //            console.log("Er: ", error)
        }
        callback(error, response)
    });
}
var rename = function (obj, prefix, _meta) {

    if (typeof obj !== 'object' || !obj) {
        return false; // check the obj argument somehow
    }

    var keys = Object.keys(obj),
        keysLen = keys.length,
        prefix = prefix || '';
    for (var i = 0; i < keysLen; i++) {
        if (_meta.apiProperties[i].datatype == 'number') {
            obj[prefix + keys[i]] = parseInt(obj[keys[i]]);
            delete obj[keys[i]];
        } else if (_meta.apiProperties[i].datatype == 'Date') {
            if (obj[keys[i]] === '0000-00-00') {
                obj[prefix + keys[i]] = new Date();
                delete obj[keys[i]];
            } else {
                // console.log("locatioin is fine", obj[keys[i]])
                obj[prefix + keys[i]] = new Date(obj[keys[i]]);
                delete obj[keys[i]];
            }
        } else {

            obj[prefix + keys[i]] = obj[keys[i]];
            delete obj[keys[i]];
        }
    }
    return obj;
}
var putprefix = function (obj, prefix) {
    if (typeof obj !== 'object' || !obj) {
        return false; // check the obj argument somehow
    }
    var keys = Object.keys(obj),
        keysLen = keys.length,
        prefix = prefix || '';
    for (var i = 0; i < keysLen; i++) {

        if (keys[i] === 'DONSET' || keys[i] === 'DNOT' || keys[i] === 'DLOPV' || keys[i] === 'DSTOOL1' || keys[i] === 'DSTOOL2') {
            if (obj[keys[i]] == '') {
                obj[prefix + keys[i]] = new Date('1111-11-11');
                delete obj[keys[i]];
            } else {
                obj[prefix + keys[i]] = new Date(obj[keys[i]]);
                delete obj[keys[i]];
            }
        } else if (keys[i] === 'UC') {
            obj[prefix + keys[i]] = obj[keys[i]].toString();
            delete obj[keys[i]];
        } else {
            obj[prefix + keys[i]] = obj[keys[i]];
            delete obj[keys[i]];
        }
    }
    return obj;

}

var afpwithDate = function (obj) {
    var keys = Object.keys(obj),
        keysLen = keys.length,
        prefix = prefix || '';
    for (var i = 0; i < keysLen; i++) {
        if (keys[i] === 'DONSET' || keys[i] === 'DLOPV' || keys[i] === 'DSTOOL1' || keys[i] === 'DSTOOL2' || keys[i] === 'DSTSENT1' || keys[i] === 'DSTSENT2' || keys[i] === 'DCRES' || keys[i] === 'DSTLAB1' || keys[i] === 'DSTLAB2' || keys[i] === 'DFUP' || keys[i] === 'DOI' || keys[i] === 'DNOT') {
            obj[keys[i]] = new Date(obj[keys[i]]);
        }
    }
    return obj;
}

function piclevels(obj, callback) {
    var result = _.pickBy(obj, function (value, key) {
        return key.startsWith("level");
    });
    callback(result)
}

function extend(obj1, obj2, callback) {
    var result = {};
    for (var key in obj1) result[key] = obj1[key];
    for (var key in obj2) result[key] = obj2[key];
    callback(result)
}

var addLocationData = function (data, prefix, afpdata, callback) {

    if (prefix === 'dn') {
        var mymatch = {
                "query": {
                    "bool": {
                        "must": {
                            "query_string": {
                                "query": "campName:SNID-OCT-16 AND level3_id:" + data[prefix + "dist_id"]
                            }
                        }
                    }
                }
            }
            //            {
            //            level3_id: data[prefix + "dist_id"],
            //            campName: "SNID-OCT-16"
            //        }
    } else if (prefix === 'lq') {
        var mymatch = {
            level5_c: data[prefix + "Ucode"],
            campName: "SNID-OCT-16"
        }
    } else if (prefix == 'env') {
        var mymatch = {
            level5_c: data["location_code"]
        }
    } else {
        var mymatch = {
            "query": {
                "bool": {
                    "must": {
                        "query_string": {
                            "query": "campName:SNID-OCT-16 AND level5_c:" + data[prefix + "location_code"]
                        }
                    }
                }
            }
        }

        //            {
        //            level5_c: data[prefix + "location_code"],
        //            campName:"SNID-OCT-16"
        //        }
    }
    es_client.search({
        index: 'eoc',
        type: 'coverage',
        body: mymatch
    }, function (error, response) {
        if (!error) {
            if (response.hits.total != 0) {
                piclevels(response.hits.hits[0]._source, function (locations) {
                    if (afpdata) {
                        extend(afpdata, locations, function (object) {
                            callback(null, object)
                        })
                    } else {

                        extend(data, locations, function (object) {
                            //                            console.log("final with location: ", object)
                            callback(null, object)
                        })
                    }
                })
            } else {
                if (afpdata) {

                    callback(null, afpdata)
                } else {
                    //                    console.log("final with NO location: ", response)
                    // console.log("location ", data[prefix + 'location_code'], " not found")
                    callback({
                        err: "location " + data[prefix + 'location_code'] + " not found"
                    }, null)
                }
            }
        } else {
            error.matching = mymatch
            callback(error, [])
        }

    });

}

var csvtoes = function (data, catagory) {
    console.log("catagory of csvs file is ", catagory)
    console.log("data lenght = ", data.length)
    if (catagory === 'afp') {
        var bar = new ProgressBar(':bar', {
            total: data.length
        });
        forEach(data, function (object, index) {

            var done1 = this.async();
            getafplocationcode(afpwithDate(object), "", function (err, obj) {
                done1();
                bar.tick();
                if (data.length === index) {
                    console.log("CSVTOES Done!")
                }
            })
        })
    } else if (catagory === 'env') {
        console.log("Env data")
        var bar = new ProgressBar(':bar', {
            total: data.length
        });
        forEach(data, function (object, index) {

            var done1 = this.async();
            getenvlocationcode(putprefix(object, catagory), catagory, function (err, obj) {
                done1();
                bar.tick();

                if (data.length === index) {
                    console.log("CSVTOES Done!")
                }
            })
        })
    } else {
        forEach(data, function (obj, idx) {
            var done = this.async();
            es_client.index({
                index: 'etltest',
                type: 'envids1',
                id: obj.id,
                body: obj
            }, function (error, response) {
                done()
                console.log(error, response)
                if (data.length == idx) {

                    console.log("CSVTOES Done!")
                }
                if (error)
                    console.log("#Err: ", error)
                else
                    console.log("Success")
            });
        })
    }

}

var getenvlocationcode = function (_obj, prefix, callback) {
    es_client.search({
        index: 'etltest',
        type: 'envids',
        body: {
            query: {
                match: {
                    epid: _obj[prefix + 'EPID']
                }
            }
        }
    }, function (error, response) {
        if (!error) {
            if (response.hits.total != 0) {
                addLocationData(response.hits.hits[0]._source, prefix, _obj, function (err, resp) {
                    if (!err) {

                        pushToES('envdataa', resp[prefix + 'EPID'], resp, function (err, obj) {
                            if (!err) {
                                callback(null, true)
                            }
                        })
                    } else {
                        console.log("Error: addlocation .. ", err)
                        callback(true, null)
                    }
                })
            }
        } else {
            console.log("Error in fetching data: ", error)
        }
    })
}

var getafplocationcode = function (obj, prefix, callback) {
    es_client.search({
        index: 'etltest',
        type: 'afpids1',
        body: {
            query: {
                match: {
                    epid: obj.EPID
                }
            }
        }
    }, function (error, response) {
        if (!error) {
            if (response.hits.total != 0) {
                addLocationData(response.hits.hits[0]._source, '', obj, function (err, resp) {
                    if (!err) {
                        pushToES('afpdata', resp.EPID, resp, function (err, _obj) {
                            if (!err) {
                                callback(null, true)
                            }
                        })
                    } else
                        callback(true, null)
                })

                // } else {
                //     console.log("location ", data[prefix + 'location_code'], " not found")
                //     callback(404, [])
                // }
            } else
                callback(error, [])
        } else {
            console.log("type: ", obj)
            console.log("EE: ", error)
        }
    });
}
var xpElasticETL = {
    dashboards: dashboards,
    exeIDIMStoES: exeIDIMStoES,
    addESCron: addESCron,
    csvtoes: csvtoes
}

module.exports = xpElasticETL
