Node.js - lbdboperations
================

Easily read/upadat loopback datasources.


Installation
------------

    npm install lbdboperations --save



API
---

### getdatasources(filename, callback)

`filename`: Pass in any `datasources.json` 


```js
var lb_ds_op = require('lbdsoperations')
lb_ds_op.getdatasources('./server/datasources.json', function(err, obj){
    console.log(obj)
  })
```


### adddatasource(filename,datasourcename, configurations)

`options`: Pass in any `fs.readFileSync` options or set `reviver` for a [JSON reviver](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/parse). Also `throws` set to `false` if you don't ever want this method
to throw on invalid JSON. Will return `null` instead. Defaults to `true`.

```js
var lb_ds_op = require('lbdsoperations')

  lb_ds_op.adddatasource('./server/datasources.json',"MES",  {
    "host": "localhost",
    "port": 5432,
    "database": "RE",
    "username": "postgres",
    "password": "root",
    "name": "MES",
    "connector": "postgresql"
  }, function(err, obj){
    console.log(err, obj)

    lb_ds_op.getdatasources('./server/datasources.json', function(err, obj){
      console.log(obj)
    })
  })
```


### writeFile(filename, [options], callback)

`options`: Pass in any `fs.writeFile` options or set `replacer` for a [JSON replacer](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify). Can also pass in `spaces`.


```js
var jsonfile = require('jsonfile')

var file = '/tmp/data.json'
var obj = {name: 'JP'}

jsonfile.writeFile(file, obj, function (err) {
  console.error(err)
})
```

**formatting with spaces:**

```js
var jsonfile = require('jsonfile')

var file = '/tmp/data.json'
var obj = {name: 'JP'}

jsonfile.writeFile(file, obj, {spaces: 2}, function(err) {
  console.error(err)
})
```

License
-------

(MIT License)

Copyright 2012-2015, Rizwan Haider  <rizwan.bsse@gmail.com>





