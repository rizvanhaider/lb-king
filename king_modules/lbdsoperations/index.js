var jsonfile = require('jsonfile')
jsonfile.spaces = 4

function getdatasources(file, callback) {
    jsonfile.readFile(file, function (err, obj) {
        callback(err, obj);
    })
};

function getDSString(file, callback) {
    jsonfile.readFile(file, function (err, obj) {
        callback(err, Object.keys(obj));
    })
};

function adddatasource(file, dsname, obj, callback) {
    getdatasources(file, function (err, data) {
        if (!err) {
            data[dsname] = obj
            jsonfile.writeFile(file, data, function (err, obj) {
                callback(err, obj)
            });
        }
    })
};
//function gets and return all models/tables of a datasource
function discModelDef(dsString, app, callback) {
    var dataSource = app.dataSources[dsString];

    dataSource.discoverModelDefinitions({
        schema: dataSource.settings.schema
    }, function (err, schema) {
        if (err) {
            console.error(err)
            callback(err, schema)
        } else{
            callback(null, schema)
        }
    });
}

// [discAndBuildModel] Builds model from existing table of any database and returns boolean (true: success, false: fail)
function discAndBuildModel(model, datasource, app, callback) {
    var dataSource = app.dataSources[datasource];
    dataSource.discoverSchema(model.name, {
        schema: dataSource.settings.schema
    }, function (err, schema) {
        if(err)
            console.log(err)
        //            var modelone = dataSource.createModel(model.schema, schema);
        //var modelone = builder.define(schema.name, schema);
        //modelone.attachTo(dataSource);
        //console.log("Auto discovery success: " + schema.name);
        var outputName = './common/models/' + schema.name + '.json';
        jsonfile.writeFile(outputName, schema, function (err) {
            if (err) {
                console.error("Error writing model file in common/", schema.name, ".json: ", err)
                callback(err, null)
            } else {
                updateModelConfigJSON(schema.name, datasource)
                callback(null, schema.name)
            }
        })


        //fs.writeFile(outputName, JSON.stringify(schema, null, 2), function (err) {
        //  if (err) {
        //    console.log(err);
        //  } else {
        //    console.log("JSON saved to " + outputName)
        //    updateModelConfigJSON(schema.name, datasource)
        //  }
        //});
    });
}

function updateModelConfigJSON(model, datasource) {
    var file = './server/model-config.json'
    jsonfile.readFile(file, function (err, obj) {
        obj[model] = {
            'dataSource': datasource
        }
        jsonfile.writeFile(file, obj, function (err) {
            if (err)
                console.error("Error updating model-config.json", err)
        })
    })
}


// Reads the properties of model
function getModelProperties(model, app, callback) {
    var dataSource = app.dataSources.MES;
    dataSource.discoverSchemas(model, function (err, schema) {
        callback(err, schema)
    });
}

var lbdsoperations = {
    spaces: null,
    getdatasources: getdatasources,
    getDSString: getDSString,
    discModelDef: discModelDef,
    getModelProperties: getModelProperties,
    updateModelConfigJSON: updateModelConfigJSON,
    discAndBuildModel: discAndBuildModel,
    adddatasource: adddatasource
}

module.exports = lbdsoperations
