require('events').EventEmitter.prototype._maxListeners = 999999;

var jsonfile = require('jsonfile')
jsonfile.spaces = 4

var addDataElements = function (app,aDatasetID, idims_api_url, callback) {
    var dataSource = app.dataSources.MES;
    var modelDataelement = dataSource.models['Dataelement']
        //rest(idims_api_url).then(function (response) {
        //var data = JSON.parse(response.entity);
        //var keys = data.data.keys
    var keys = [
      "ID",
      "IDcampCat",
      "ActivityID",
      "Yr",
      "TimeStamp",
      "DateAssessment",
      "DaysBeforeActivity",
      "TehsilID",
      "UCID",
      "targeted",
      "inaccCh",
      "areasInacc",
      "mobileTeams",
      "fixedTeams",
      "TransitPoints",
      "areaIcs",
      "aicTrained",
      "zonalSupervisor",
      "mpValid",
      "mpfv",
      "mpfvDstaff",
      "alltm18yr",
      "govtWorker",
      "mtLocalmem",
      "mtFemale",
      "alltmTrg",
      "ddmCard",
      "UpecHeld",
      "UpecDate",
      "ucmo",
      "ucSecratry",
      "shoUpec",
      "scVerified",
      "remarks",
      "status",
      "trash",
      "location_code"
    ]
    var dataElementIDs = []
    var keysize = keys.length
    keys.forEach(function (key) {
        keysize--
        modelDataelement.create({
            "name": key,
            "shortname": key,
            "description": key,
            "valuetype": "string",
            "aggregationtype": "string",
            "parentid": 0,
            "disaggregationCombinationid": 0,
            "url": idims_api_url,
            "domaintype": "string",
            "lastupdated": new Date().toUTCString(),
            "zeroissignificant": true,
            "formname": "string",
            "uid": "-1",
            "optionsetid": 0,
            "legendsetid": 0,
            "created": new Date().toUTCString(),
            "userid": 0,
            "publicaccess": "string",
            "commentoptionsetid": 0
        }, function (err, responce) {
            if (!err) {
                modelDataelement.findOne({
                    where: {
                        name: key
                    }
                }, function (err, resp) {
                    dataElementIDs.push(resp.dataelementid)
                    addDatasetmembers(dataSource, aDatasetID, resp.dataelementid, callback)
                  if(keysize ===0)
                  callback(err, keys.length)
                    //console.log(dataElementIDs, "END^^^")
                })
            } else {
                console.log("Err: ", err)
                callback(err, dataElementIDs)
            }
        })
        console.log(key, 'line:87')
    })
}

var addDataSet = function (app, dataset, callback) {
    var dataSource = app.dataSources.MES;
    var modelDataset = dataSource.models['Dataset']
    modelDataset.create({
        "name": dataset,
        "periodtypeid": 0,
        "shortname": dataset,
        "lastupdated": new Date().toUTCString(),
        "description": "automigrated",
        "created": new Date().toUTCString()
    }, function (err, responce) {
        if (!err) {
            console.log(dataset, "added dataset: ", dataset)
            modelDataset.findOne({
                where: {
                    name: responce.name
                }
            }, function (err, resp) {
                console.log("Updated data = ", resp)
                callback(null, resp.datasetid)
            })
        } else {
            callback(err, null)
            console.error(err)
        }
    })
}

  function addDatasetmembers(dataSource, aDatasetID, aDataelementID, callback){
    var modelDatasetmembers = dataSource.models['Datasetmembers']
    modelDatasetmembers.create({
      "datasetid": aDatasetID,
      "dataelementid": aDataelementID
    } , function(err, responce){
//      if(err)
        callback(err, responce)
//      else
//        callback(null, responce)
    })
  }

var idimstopms = {

    addDataElements: addDataElements,
    addDataSet: addDataSet
}

module.exports = idimstopms
