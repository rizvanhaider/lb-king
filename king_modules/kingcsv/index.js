//var Converter = require("csvtojson").Converter;
//var converter = new Converter({});


var Converter=require("csvtojson").Converter;

var fs=require("fs"); 
//CSV File Path or CSV String or Readable Stream Object
var csvFileName="./csvcontainer/csvs/";

//new converter instance
var csvConverter=new Converter();




var readCSVfile = function (file, callback) {
    var converter=new Converter();
    converter.fromFile('./csvcontainer/csvs/'+file,function(err,result){
        if(!err)
            callback(null, result)
        else
            callback(err, result)
    });

}

var readCSVHeaders = function(file, callback){
    //new converter instance
var csvConverter=new Converter();
    console.log("Reading file: " + csvFileName+file)

    csvConverter.on("error", function(err){
        console.error("KINGCSV: Error in reading csv file", err)
    })
    
    csvConverter.on("record_parsed",function(resultRow, rawRow, rowIndex){
           if(rowIndex>0)
                return
        callback(null, Object.keys(resultRow));
    });
    
    fs.createReadStream(csvFileName+file).pipe(csvConverter);
    
    
//    converter.fromFile('./csvcontainer/csvs/'+file,function(err,result){
////        console.log('kingcsv: ', Object.keys(result[0]))
//      if(!err)
//            callback(null, Object.keys(result[0]))
//       else{
//           console.error("Eror in kincsv", err)
//            callback(err, "cannot read file")
//       }
//    });
}

var kingcsv = {
    readCSVfile: readCSVfile,
    readCSVHeaders: readCSVHeaders
}

module.exports = kingcsv
