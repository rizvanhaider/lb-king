# Configurations
Clone the project into your directory
run command "npm install" in main directory
run command "bower install" in "client" directory

finally run the project by "node ." or "nodemon ."
it should be running on "http://localhost:3000"

# Things to Remember

Add an extra property 'schema' in datasources.json while adding new datasource, 
e.g. "schema": "database-name"