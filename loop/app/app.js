/*PACKAGES*/
var express = require('express');
var fetch = require('node-fetch');
var db = require('./db');
var helper = require('./helper');
var request = require("request");
var csv = require("fast-csv");
//var fs=require("fs");

var app = express();
var j = {
    "code": "200",
    "status": "1",
    "data": {
        "keys": [
      "ID",
      "IDcampCat",
      "ActivityID",
      "Yr",
      "TimeStamp",
      "Cday",
      "TehsilID",
      "UCID",
      "ccpvTargets",
      "VaccinationDate",
      "TeamsRpt",
      "Age011",
      "Age1259",
      "NA",
      "Refusals",
      "CovNA",
      "CovRef",
      "CovMob",
      "CovTransit",
      "HHvisit",
      "MultipleFamily",
      "Weakness",
      "ZeroRt",
      "OPVGiven",
      "OPVUsed",
      "OPVReturned",
      "Stage34",
      "Inaccessible",
      "Remarks",
      "status",
      "trash",
      "location_code",
      "campcat_name"
    ],
        "data": [
            {
                "ID": "128431",
                "IDcampCat": "2",
                "ActivityID": "3",
                "Yr": "2016",
                "TimeStamp": "2016-05-11 19:55:16",
                "Cday": "1",
                "TehsilID": "344",
                "UCID": "5736",
                "ccpvTargets": "0",
                "VaccinationDate": "2016-05-11",
                "TeamsRpt": "26",
                "Age011": "178",
                "Age1259": "911",
                "NA": "148",
                "Refusals": "89",
                "CovNA": "7",
                "CovRef": "11",
                "CovMob": "58",
                "CovTransit": "0",
                "HHvisit": "276",
                "MultipleFamily": "0",
                "Weakness": "0",
                "ZeroRt": "0",
                "OPVGiven": "1300",
                "OPVUsed": "1250",
                "OPVReturned": "50",
                "Stage34": "0",
                "Inaccessible": "0",
                "Remarks": "",
                "status": "0",
                "trash": "0",
                "location_code": "41401007",
                "campcat_name": "CCPV"
      },
            {
                "ID": "128432",
                "IDcampCat": "2",
                "ActivityID": "3",
                "Yr": "2016",
                "TimeStamp": "2016-05-11 19:59:25",
                "Cday": "1",
                "TehsilID": "344",
                "UCID": "5743",
                "ccpvTargets": "0",
                "VaccinationDate": "2016-05-11",
                "TeamsRpt": "28",
                "Age011": "373",
                "Age1259": "1142",
                "NA": "47",
                "Refusals": "56",
                "CovNA": "21",
                "CovRef": "15",
                "CovMob": "24",
                "CovTransit": "0",
                "HHvisit": "405",
                "MultipleFamily": "0",
                "Weakness": "0",
                "ZeroRt": "0",
                "OPVGiven": "1800",
                "OPVUsed": "1750",
                "OPVReturned": "50",
                "Stage34": "0",
                "Inaccessible": "0",
                "Remarks": "",
                "status": "0",
                "trash": "0",
                "location_code": "41401014",
                "campcat_name": "CCPV"
      },
    ]
    }
};
var final = [];
var api_data = [];
var api_data_length = 0;


app.get('/api/mysqk', function (req, res) {
    var e = "BN/41/08/018";
    db.my(e, function (data) {
        console.log(data);
        db.loc(data[0].ucode, function (data) {
            res.json(data);
        });
    })
})

app.get('/', function (req, res) {
    res.send('Hello World!');
});

app.get('/api/controllroom/:id', function (req, res, next) {
    db.test(req.params.id, function (data) {
        res.json(data);
    });
});



app.get('/api/graphs/:id', function (req, res, next) {
    var test = [{
            "categories": ["SNID-FEB-16", "NID-MAR-16", "SNID-APR-16"],
            "title": "Comparison of Still Missed (%) from difference Sources of data",
            "ytext": "% of children"

},
        {
            "name": "Administrative",
            "data": [10, 2, 10]
        }, {
            "name": "Market Survey",
            "data": [4, 3.8, 4.4]
        },
        {
            "name": "3rd Party (PMC)",
            "data": [6, 4, 0]
        }];
    helper.jsreport(req.params.id, test, function (data) {
        res.send(data);
    })
});





function fetch_next() {
    data_parser(api_data[pointer]);
//    data_parser_afp(api_data[pointer]);
}

function data_parser(event) {

    for (key in event) {
        if (key === "campcat_name" || key === "Remarks" || key === "VaccinationDate" || key === "TimeStamp") {
            //event[key]=parseInt(event[key]);
        } else if (event.VaccinationDate == "0000-00-00") {
            event.VaccinationDate = "2016-02-02"
        }
		else if (key === "day1") {
            var date = event.day1;
            event.day1 = date;
//            var date = new Date(event.day1);
//            event.day1 = date;
        }else if (key === "dpec") {
			var date = event.dpec;
            event.dpec = date;
//			var date = new Date(event.dpec);
//            event.dpec = date;
        }
		else if(key==="NA"){
			 var temobj="MSNA";
			event[temobj]=parseInt(event.NA);
			delete(event.NA);


		}

		else if(key==="Refusals"){
			 var temobj="MSRefusals";
			event[temobj]=parseInt(event.Refusals);
			delete(event.Refusals);


		}
		else {
            event[key] = parseInt(event[key]);
        }
    }


    db.loc(event.location_code, function (data) {
        for (key in data) {
            event[key] = data[key];
        }
        db.dump(event, function (data) {
            console.log(pointer);
            pointer++;
            fetch_next();
        });
    });
}

app.get('/api/test/', function (req, res, next) {
//    var url = "http://58.65.177.12/api_who/api/get_allcontrollroom/5468XE2LN6CzR7qRG041/3/2016"; //control room May
//    var url = "http://58.65.177.12/api_who/api/get_allcontrollroom/5468XE2LN6CzR7qRG041/2/2016"; //control room May
//    var url = "http://58.65.177.12/api_who/api/get_allcontrollroom/5468XE2LN6CzR7qRG041/1/2016"; //control room May
//        var url = "http://58.65.177.12/api_who/api/get_allcatchup/5468XE2LN6CzR7qRG041/2/2016";   //catchup MAY
//        var url = "http://58.65.177.12/api_who/api/get_allcatchup/5468XE2LN6CzR7qRG041/3/2016";   //catchup MAY
//       var url = "http://58.65.177.12/api_who/api/get_allaftercatchup/5468XE2LN6CzR7qRG041/3/2016"; // post catchup may ////Pend
//       var url = "http://58.65.177.12/api_who/api/get_allaftercatchup/5468XE2LN6CzR7qRG041/3/2016"; // post catchup may
//       var url = "http://58.65.177.12/api_who/api/get_allaftercatchup/5468XE2LN6CzR7qRG041/3/2016"; // post catchup may
//       var url = "http://58.65.177.12/api_who/api/get_allneap/5468XE2LN6CzR7qRG041/3/2016"; // All Neap
//       var url = "http://58.65.177.12/api_who/api/get_allmarketSurvey/5468XE2LN6CzR7qRG041/3/2016"; // Market Survey
	var url = "http://58.65.177.12/api_who/api/get_allplaningDist/5468XE2LN6CzR7qRG041/4/2016";

    request({
        url: url,
        json: true
    }, function (error, response, body) {

        if (!error && response.statusCode === 200) {
            res.json(body.data.data.length);
            var l = body.data.data.length;
            var c = 1;
            console.log(l);
            api_data = body.data.data;
            pointer = 0;
            fetch_next();
        }
    })

});

app.get('/api/t/', function (req, res, next) { // or make each task as seprate callback function
    var c = 1;
    var l = j.data.data.length
    j.data.data.forEach(function (event) {
        for (key in event) {
            if (key === "campcat_name" || key === "Remarks" || key === "VaccinationDate" || key === "TimeStamp") {
                //event[key]=parseInt(event[key]);
            } else {
                event[key] = parseInt(event[key]);
            }
        }
        db.loc(event.location_code, function (data) {
            for (key in data) {
                event[key] = data[key];
            }
            db.dump(event, function (data) {
                if (c == l) {
                    console.log("completed=========================================================");
                } else {
                    console.log(c + " out of " + l);
                    c++;
                }
            });
        });
    });
});

app.get('/api/location/:id', function (req, res, next) {
    db.loc(req.params.id, function (data) {
        res.json(data);
    });
});

app.get('/api/controllroom', function (req, res, next) {
    console.log("called");
    var Converter = require("csvtojson").Converter;
    var converter = new Converter({});
    converter.on("end_parsed", function (jsonArray) {

        api_data = jsonArray;
        api_data_length = api_data.length;
        pointer = 0;
        fetch_next();

    });

    require("fs").createReadStream("file.csv").pipe(converter);


});

app.get('/api/period/:id', function (req, res, next) {
    db.period(req.params.id, function (data) {
        res.json(data);
    });
});

app.get('/api/es/', function (req, res, next) {
    db.es(function (data) {
        console.log(data);
    });
});


/*AFP Api*/
function date_formmating(obj, callback) {
    var date = new Date(obj);
    callback(date);
}

function conversion(event, callback) {
    var date_keys = ["DFUP", "DSTLAB2", "DSTLAB1", "DCRES", "DSTSENT2", "DSTSENT1", "DSTOOL2", "DSTOOL1", "DOI", "DNOT", "DONSET", "DENTER"];
    for (key in event) {
        if (date_keys.indexOf(key) >= 0) { // for data conversion
            if (event[key] == "") {
                event[key] = null;
            } else {
                date_formmating(event[key], function (data) {
                    event[key] = data;
                })
            }
        }
    }
    callback(event);
}





function data_parser_afp(event) {
    conversion(event, function (event) {
        db.my(event.EPID, function (data) {
            if (data.length > 0) { // epid match fetch location
                db.loc(data[0].ucode, function (data) {
                    for (key in data) {
                        event[key] = data[key];
                    }
                    db.dump_afp(event, function (data) {
                        console.log(pointer + " out of " + api_data_length);
                        pointer++;
                        if (pointer < api_data_length) {
                            fetch_next();
                        } else {
                            console.log("completed");
                        }
                    });
                });
            } else if (data != "error") { // epid not match just dump event without location
                db.dump_afp(event, function (data) {
                    console.log(pointer + " out of " + api_data_length);
                    pointer++;
                    if (pointer < api_data_length) {
                        fetch_next();
                    } else {
                        console.log("completed");
                    }
                });

                /*if(data!="error"){
				        db.myi(event.EPID,function(){
						    console.log("Missing",event.EPID);
				        });
				    }
 					pointer++;
					if(pointer<api_data_length){
						fetch_next();
					}			*/
            }
        });
    });
}

app.get('/api/afp', function (req, res, next) {
    console.log("called");
    var Converter = require("csvtojson").Converter;
    var converter = new Converter({});
    converter.on("end_parsed", function (jsonArray) {

        api_data = jsonArray;
        api_data_length = api_data.length;
        pointer = 0;
        fetch_next();


    });

    require("fs").createReadStream("afp.csv").pipe(converter);


});









app.listen(3007, function () {
    console.log('Example app listening on port 3007!');
});
